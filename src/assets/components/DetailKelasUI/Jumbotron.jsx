import {
  Box,
  Button,
  FormControl,
  Grid,
  InputLabel,
  Select,
  Stack,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import MenuItem from "@mui/material/MenuItem";
import { rupiah } from "../../../utility/formatIDR";
import moment from "moment/moment";
import axios from "axios";
import { Notif } from "../Notif";
import { useNavigate } from "react-router";

export default function Jumbotron({ value, user, token }) {
  const [selectedDate, setSelectedDate] = useState("");
  const [dates, setDates] = useState([]);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarColor, setSnackbarColor] = useState("");
  const navigate = useNavigate();

  const addToCartHandler = () => {
    if (!selectedDate) {
      // Tampilkan alert atau notifikasi bahwa pengguna harus memilih jadwal
      setSnackbarMessage("selected schedule harus di isi !");
      setSnackbarColor("red");
      setSnackbarOpen(true);
      return;
    }
    const apilink = process.env.REACT_APP_BASE_URL;
    let config = {
      method: "post",
      maxBodyLength: Infinity,
      url: `${apilink}/Cart`,
      headers: {
        "Content-Type": "application/json",
      },
      data: {
        idUser: user,
        idCourse: value.id,
        courseName: value.name,
        courseCategory: value.category,
        courseImage: value.image,
        coursePrice: value.price,
        date: selectedDate,
      },
    };
    axios
      .request(config)
      .then((response) => {
        setSnackbarMessage("Successfully added to your cart!");
        setSnackbarColor("green");
        setSnackbarOpen(true);
      })
      .catch((error) => {
        if (error.response && error.response.data) {
          setSnackbarMessage(error.response.data);
          setSnackbarColor("red");
        } else {
          setSnackbarMessage("An error occurred while adding the user.");
          setSnackbarColor("red");
        }
        setSnackbarOpen(true);
      });
  };

  const buyNowHandler = () => {
    if (!selectedDate) {
      // Tampilkan alert atau notifikasi bahwa pengguna harus memilih jadwal
      setSnackbarMessage("selected schedule harus di isi !");
      setSnackbarColor("red");
      setSnackbarOpen(true);
      return;
    } else {
      addToCartHandler();
      navigate("/checkout");
    }
  };

  useEffect(() => {
    const currentDate = moment();
    const generatedDates = generateDates(currentDate, 6);
    setDates(generatedDates);
  }, []);

  const generateDates = (startDate, numDays) => {
    const generatedDates = [];
    let currentDate = moment(startDate).add(1, "day");

    while (generatedDates.length < numDays) {
      if (currentDate.day() !== 0) {
        generatedDates.push(currentDate.clone());
      }

      currentDate.add(1, "day");
    }

    return generatedDates;
  };

  const handleChange = (event) => {
    setSelectedDate(event.target.value);
  };
  const handleNotif = () => {
    setSnackbarOpen(false);
  };
  return (
    <>
      <Notif
        snackbarOpen={snackbarOpen}
        handleNotif={handleNotif}
        snackbarColor={snackbarColor}
        snackbarMessage={snackbarMessage}
      />
      <Box
        sx={{
          marginTop: "80px",
          marginLeft: "40px",
          marginBottom: "75px",
          marginRight: "40px",
        }}>
        <Box>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={4}>
              <img
                src={value.image}
                alt=""
                style={{
                  width: "100%", // Set lebar gambar menjadi 100% agar sesuai dengan lebar kontainer
                  maxWidth: "400px", // Batasi lebar maksimum gambar agar tidak terlalu besar
                  height: "auto",
                  // Biarkan tinggi gambar menyesuaikan agar tidak terdistorsi
                }}
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <Box
                display="flex"
                flexDirection="column"
                alignItems="flex-start">
                <Typography>{value.category}</Typography>
                <Typography
                  variant="h1"
                  sx={{
                    fontSize: "24px",
                    fontWeight: "600",
                    marginTop: "8px",
                  }}>
                  {value.name}
                </Typography>
                <Typography
                  variant="h2"
                  fontSize={"24px"}
                  fontWeight={600}
                  marginTop={2}
                  sx={{ color: "#790B0A" }}>
                  {rupiah(value.price)}
                </Typography>

                {!token ? null : (
                  <>
                    <FormControl
                      sx={{
                        width: "300px",
                        marginTop: "32px",
                        height: "40px",
                      }}
                      size="small">
                      <InputLabel>Select Schedule</InputLabel>
                      <Select
                        value={selectedDate}
                        onChange={handleChange}
                        sx={{ color: "#41454D", fontSize: "15px" }}>
                        {dates.map((date, index) => (
                          <MenuItem key={index} value={date}>
                            {date.format("dddd, DD MMMM YYYY")}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    <Stack
                      spacing={2}
                      direction={{ xs: "column", sm: "row" }}
                      sx={{ marginTop: "40px" }}>
                      <Button
                        variant="outlined"
                        onClick={() => addToCartHandler()}
                        sx={{
                          width: "230px",
                          height: "40px",
                          color: "#790B0A",
                          borderColor: "#790B0A",
                          fontSize: "16px",
                          borderRadius: "8px",
                        }}>
                        Add to Cart
                      </Button>
                      <Button
                        onClick={() => buyNowHandler()}
                        variant="contained"
                        sx={{
                          width: "230px",
                          height: "40px",
                          color: "#fff",
                          borderColor: "#790B0A",
                          fontSize: "16px",
                          backgroundColor: "#790B0A",
                          borderRadius: "8px",
                        }}>
                        Buy Now
                      </Button>{" "}
                    </Stack>
                  </>
                )}
              </Box>
            </Grid>
          </Grid>
        </Box>
        <Box marginTop={"40px"} textAlign={"justify"}>
          <Typography fontSize={24} fontWeight={600}>
            Description
          </Typography>
          <Typography>{value.description}</Typography>
        </Box>
      </Box>
    </>
  );
}
