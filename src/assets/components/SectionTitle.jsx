import React from "react";
import { Typography } from "@mui/material";

const SectionTitle = ({ text }) => {
  return (
    <>
      <Typography
        variant="h4"
        component="p"
        fontWeight="bold"
        color="#790B0A"
        align="center"
        sx={{ marginBottom: "80px", marginTop: "40px" }}>
        {text}
      </Typography>
    </>
  );
};

export default SectionTitle;
