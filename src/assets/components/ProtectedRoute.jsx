import React, { useContext } from "react";
import { Navigate } from "react-router-dom";
import { AuthContext } from "../../context/AuthProvider";

const ProtectedRoute = ({ element: Element, roles, redirectTo }) => {
  const { token, user } = useContext(AuthContext);

  if (!token || !user || (roles && !roles.includes(user.role))) {
    return <Navigate to={redirectTo} />;
  }

  return Element;
};

export default ProtectedRoute;
