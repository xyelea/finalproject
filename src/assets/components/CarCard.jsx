import React from "react";
import { Card, CardContent, CardMedia, Typography } from "@mui/material";
import { rupiah } from "../../utility/formatIDR";

const CarCard = ({ image, carType, price, carName }) => {
  return (
    <Card sx={{ margin: "10px" }}>
      <CardMedia
        component="img"
        sx={{
          objectFit: "cover",
          width: "100%",
          height: "100%",
          border: "none",
        }}
        image={image}
        alt="Gambar Mobil"
      />
      <CardContent>
        <Typography variant="subtitle2" color="GrayText">
          {carType}
        </Typography>
        <Typography variant="subtitle1" fontWeight="bold">
          {carName}
        </Typography>
        <Typography
          variant="subtitle1"
          color="#790B0A"
          fontWeight="bold"
          sx={{ marginTop: "50px" }}
        >
          {rupiah(price)}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default CarCard;
