import TextField from "@mui/material/TextField";
import React from "react";

const TextFieldComponent = ({ label, type, value, onChange }) => {
  return (
    <TextField
      label={label}
      variant="outlined"
      type={type}
      value={value}
      onChange={onChange}
      sx={{ width: "100%", my: 2 }}
    />
  );
};

export default TextFieldComponent;
