import AccountBoxIcon from "@mui/icons-material/AccountBox";
import DashboardIcon from "@mui/icons-material/Dashboard";
import LogoutIcon from "@mui/icons-material/Logout";
import PaymentsIcon from "@mui/icons-material/Payments";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import DescriptionIcon from "@mui/icons-material/Description";
import { Link, useNavigate } from "react-router-dom";
import { useContext, React } from "react";
import { AuthContext } from "../../context/AuthProvider";
import { List } from "@mui/material";

export const MainListItems = () => {
  return (
    <List>
      <ListSubheader component="div" inset>
        Main Menu
      </ListSubheader>
      <ListItemButton component={Link} to="/adminpanel">
        <ListItemIcon>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </ListItemButton>
      <ListItemButton component={Link} to="/adminpanel/user-management">
        <ListItemIcon>
          <AccountBoxIcon />
        </ListItemIcon>
        <ListItemText primary="User Management" />
      </ListItemButton>
      <ListItemButton component={Link} to="/adminpanel/payment-method">
        <ListItemIcon>
          <PaymentsIcon />
        </ListItemIcon>
        <ListItemText primary="Payment Method" />
      </ListItemButton>
      <ListItemButton component={Link} to="/adminpanel/invoice-users">
        <ListItemIcon>
          <DescriptionIcon />
        </ListItemIcon>
        <ListItemText primary="Invoice Users" />
      </ListItemButton>
    </List>
  );
};
