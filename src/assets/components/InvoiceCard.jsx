import React, { useEffect, useState } from "react";
import { Grid, Typography } from "@mui/material";
import ButtonComponent from "./Button";
import { useNavigate } from "react-router-dom";
import { rupiah } from "../../utility/formatIDR";
import moment from "moment";

const InvoiceCard = ({ no, noinvoice, date, tcourse, tprice }) => {
  const [is1000W, setIs1000W] = useState(window.innerWidth <= 1000);
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);
  const navigate = useNavigate();

  useEffect(() => {
    const handleResize = () => {
      setIs1000W(window.innerWidth <= 1000);
      setIs600W(window.innerWidth <= 600);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Grid
      item
      display="flex"
      justifyContent="space-between"
      flexDirection={is600W ? "column" : "row"}
      paddingLeft="20px"
      paddingRight="20px"
      bgcolor={no % 2 === 0 ? "rgba(121, 11, 10, 0.1)" : ""}
      borderRadius={is600W ? "8px" : "4px"}
      height={is600W ? "180px" : "77px"}
      alignItems={is600W ? "flex-start" : "center"}
      width="100%"
      boxShadow={is600W ? "0px 2px 4px rgba(0, 0, 0, 0.5)" : ""}
      mb={is600W ? "10px" : ""}
    >
      <Grid
        item
        xs={is600W ? 12 : 2}
        md={is600W ? 12 : 1}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
        mt={is600W ? "20px" : null}
      >
        {is600W ? <Typography color="#4F4F4F">No :</Typography> : null}
        <Typography color="#4F4F4F" align="left">
          {no}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display={"flex"}
        justifyContent={is600W ? "space-between" : "center"}
        width={"100%"}
      >
        {is600W ? <Typography color="#4F4F4F">No Invoice :</Typography> : null}
        <Typography color="#4F4F4F" align="center">
          {noinvoice}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width={"100%"}
      >
        {is600W ? <Typography color="#4F4F4F">Date :</Typography> : null}
        <Typography color="#4F4F4F" align="center">
          {moment(date).format("D MMMM YYYY")}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display={"flex"}
        justifyContent={is600W ? "space-between" : "center"}
        width={"100%"}
      >
        {is600W ? (
          <Typography color="#4F4F4F">Total Course :</Typography>
        ) : null}
        <Typography color="#4F4F4F" align="center">
          {tcourse}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display={"flex"}
        justifyContent={is600W ? "space-between" : "center"}
        width={"100%"}
      >
        {is600W ? <Typography color="#4F4F4F">Total Price :</Typography> : null}
        <Typography color="#4F4F4F" align="center">
          {rupiah(tprice)}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width={"100%"}
        mb={is600W ? "5px" : ""}
      >
        {is600W ? (
          <Grid item xs={4} md={2}>
            <Typography color="#4F4F4F">Action :</Typography>
          </Grid>
        ) : null}
        <ButtonComponent
          onClick={() => navigate(`/detail-invoice/${noinvoice}`)}
          text="Details"
          variant="outlined"
          align="center"
          cStyle={{ width: is1000W ? "100px" : "180px" }}
        />
      </Grid>
    </Grid>
  );
};

export default InvoiceCard;

export const AdminInvoice = ({ no, id, date, tcourse, price }) => {
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);
  const navigate = useNavigate();
  useEffect(() => {
    const handleResize = () => {
      setIs600W(window.innerWidth <= 600);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Grid
      item
      display="flex"
      justifyContent="space-between"
      flexDirection={is600W ? "column" : "row"}
      paddingLeft="20px"
      paddingRight="20px"
      bgcolor={no % 2 === 0 ? "rgba(121, 11, 10, 0.1)" : ""}
      borderRadius={is600W ? "8px" : "4px"}
      height={is600W ? "180px" : "77px"}
      alignItems={is600W ? "flex-start" : "center"}
      width="100%"
      boxShadow={is600W ? "0px 2px 4px rgba(0, 0, 0, 0.5)" : ""}
      mb={is600W ? "10px" : ""}
    >
      <Grid
        item
        xs={is600W ? 12 : 2}
        md={is600W ? 12 : 1}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
        mt={is600W ? "5px" : null}
      >
        {is600W && <Typography color="#4F4F4F">No :</Typography>}
        <Typography color="#4F4F4F" align={"left"}>
          {no}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? <Typography color="#4F4F4F">No Invoice :</Typography> : null}
        <Typography color="#4F4F4F" align="center">
          {id}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? <Typography color="#4F4F4F">Date :</Typography> : null}
        <Typography color="#4F4F4F" align="center">
          {date}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? (
          <Typography color="#4F4F4F">Total Course :</Typography>
        ) : null}
        <Typography color="#4F4F4F" align="center">
          {tcourse}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? <Typography color="#4F4F4F">Total Price :</Typography> : null}
        <Typography color="#4F4F4F" align="center">
          {rupiah(price)}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
        mb={is600W ? "5px" : ""}
      >
        {is600W ? (
          <Grid item xs={4} md={2}>
            <Typography color="#4F4F4F">Action :</Typography>
          </Grid>
        ) : null}
        <ButtonComponent
          text="Details"
          variant="outlined"
          align="center"
          onClick={() => navigate(`/detail-invoice/${id}`)}
          cStyle={{ width: "100px", height: "30px" }}
        />
      </Grid>
    </Grid>
  );
};

export const AdminInvoicePanel = ({ no, id, date, tcourse, price }) => {
  const [is1000W, setIs1000W] = useState(window.innerWidth <= 1000);
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);

  useEffect(() => {
    const handleResize = () => {
      setIs1000W(window.innerWidth <= 1000);
      setIs600W(window.innerWidth <= 600);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Grid
      item
      display="flex"
      justifyContent={"space-between"}
      paddingLeft="20px"
      paddingRight="20px"
      bgcolor={no % 2 === 0 ? "rgba(121, 11, 10, 0.1)" : ""}
      borderRadius={"4px"}
      height={"77px"}
      alignItems="center"
      width="100%"
    >
      <Grid
        item
        xs={2}
        md={1}
        display={"flex"}
        justifyContent={"start"}
        width={"100%"}
      >
        <Typography color="#4F4F4F" align="left">
          {no}
        </Typography>
      </Grid>
      <Grid
        item
        xs={4}
        md={2}
        display={"flex"}
        justifyContent={"center"}
        width={"100%"}
      >
        <Typography color="#4F4F4F" align="center">
          {id}
        </Typography>
      </Grid>
      <Grid
        item
        xs={4}
        md={2}
        display={"flex"}
        justifyContent={"center"}
        width={"100%"}
      >
        <Typography color="#4F4F4F" align="center">
          {date}
        </Typography>
      </Grid>
      <Grid
        item
        xs={4}
        md={2}
        display={"flex"}
        justifyContent={"center"}
        width={"100%"}
      >
        <Typography color="#4F4F4F" align="center">
          {tcourse}
        </Typography>
      </Grid>
      <Grid
        item
        xs={4}
        md={2}
        display={"flex"}
        justifyContent={"center"}
        width={"100%"}
      >
        <Typography color="#4F4F4F" align="center">
          {rupiah(price)}
        </Typography>
      </Grid>
    </Grid>
  );
};

export const InvoiceCardUserManagement = ({
  id,
  email,
  username,
  role,
  isActive,
  handleOpenDialogEdit,
}) => {
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);

  useEffect(() => {
    const handleResize = () => {
      setIs600W(window.innerWidth <= 600);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <Grid
      item
      display="flex"
      justifyContent="space-between"
      flexDirection={is600W ? "column" : "row"}
      paddingLeft="20px"
      paddingRight="20px"
      borderRadius={is600W ? "8px" : "4px"}
      height={is600W ? "180px" : "60px"}
      alignItems={is600W ? "flex-start" : "center"}
      width={"100%"}
      boxShadow={is600W ? "0px 2px 4px rgba(0, 0, 0, 0.5)" : ""}
      mb={is600W ? "10px" : ""}
      mt={is600W ? "10px" : "20px"}
      key={id}
    >
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 5}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
        mt={is600W ? "5px" : null}
      >
        {is600W && <Typography color="#4F4F4F">Email :</Typography>}
        <Typography align="center">{email}</Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? <Typography color="#4F4F4F">Name :</Typography> : null}
        <Typography align="center">{username}</Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? <Typography color="#4F4F4F">Rule :</Typography> : null}
        <Typography align="center">
          {role && role.charAt(0).toUpperCase() + role.slice(1)}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? <Typography color="#4F4F4F">Status :</Typography> : null}
        <Grid
          item
          bgcolor={isActive ? "green" : "red"}
          color={"#fff"}
          justifyContent={"center"}
          width={"100px"}
          height={is600W ? "30px" : "40px"}
          display="flex"
          borderRadius={"5px"}
        >
          <Typography display={"flex"} align="center" alignItems={"center"}>
            {isActive ? "Active" : "Inactive"}
          </Typography>
        </Grid>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
        mb={"5px"}
      >
        {is600W ? <Typography color="#4F4F4F">Action :</Typography> : null}
        <ButtonComponent
          text="Edit"
          variant="contained"
          to={null}
          onClick={handleOpenDialogEdit}
          cStyle={{
            bgcolor: "orange",
            "&:hover": {
              backgroundColor: "orange",
            },
            height: "30px",
          }}
        />
      </Grid>
    </Grid>
  );
};

export const CardPreviewAdmin = ({ id, email, username, role, isActive }) => {
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);

  useEffect(() => {
    const handleResize = () => {
      setIs600W(window.innerWidth <= 600);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <Grid
      item
      display="flex"
      justifyContent="space-between"
      flexDirection={is600W ? "column" : "row"}
      paddingLeft="20px"
      paddingRight="20px"
      borderRadius={is600W ? "8px" : "4px"}
      height={is600W ? "180px" : "60px"}
      alignItems={is600W ? "flex-start" : "center"}
      width={"100%"}
      boxShadow={is600W ? "0px 2px 4px rgba(0, 0, 0, 0.5)" : ""}
      mb={is600W ? "10px" : ""}
      mt={is600W ? "" : "20px"}
      key={id}
    >
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 5}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
        mt={is600W ? "5px" : null}
      >
        {is600W && <Typography color="#4F4F4F">Email :</Typography>}
        <Typography align="center">{email}</Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? <Typography color="#4F4F4F">Name :</Typography> : null}
        <Typography align="center">{username}</Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? <Typography color="#4F4F4F">Rule :</Typography> : null}
        <Typography align="center">
          {role && role.charAt(0).toUpperCase() + role.slice(1)}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? <Typography color="#4F4F4F">Status :</Typography> : null}
        <Grid
          item
          bgcolor={isActive ? "green" : "red"}
          color={"#fff"}
          justifyContent={"center"}
          width={"100px"}
          height={is600W ? "30px" : "40px"}
          display="flex"
          borderRadius={"5px"}
        >
          <Typography display={"flex"} align="center" alignItems={"center"}>
            {isActive ? "Active" : "Inactive"}
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};

export const InvoiceDetailCard = ({
  no,
  courseName,
  courseCategory,
  date,
  price,
}) => {
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);
  useEffect(() => {
    const handleResize = () => {
      setIs600W(window.innerWidth <= 600);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <Grid
      item
      display="flex"
      justifyContent="space-between"
      flexDirection={is600W ? "column" : "row"}
      paddingLeft="20px"
      paddingRight="20px"
      bgcolor={no % 2 === 0 ? "rgba(121, 11, 10, 0.1)" : ""}
      height={is600W ? "180px" : "66px"}
      alignItems={is600W ? "flex-start" : "center"}
      width="100%"
      borderRadius={is600W ? "8px" : "4px"}
      boxShadow={is600W ? "0px 2px 4px rgba(0, 0, 0, 0.5)" : ""}
      mb={is600W ? "10px" : ""}
    >
      <Grid
        item
        xs={is600W ? 12 : 2}
        md={is600W ? 12 : 1}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width="100%"
      >
        {is600W ? <Typography color="#4F4F4F">No :</Typography> : null}
        <Typography>{no}</Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 10}
        display={"flex"}
        justifyContent={is600W ? "space-between" : "center"}
        width={"100%"}
      >
        {is600W ? <Typography color="#4F4F4F">Course Name :</Typography> : null}
        <Typography align="center">{courseName}</Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width={"100%"}
      >
        {is600W ? <Typography color="#4F4F4F">Type :</Typography> : null}
        <Typography align="center">{courseCategory}</Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width={"100%"}
      >
        {is600W ? <Typography color="#4F4F4F">Schedule:</Typography> : null}
        <Typography align="center">
          {moment(date).format("dddd, D MMMM YYYY")}
        </Typography>
      </Grid>
      <Grid
        item
        xs={is600W ? 12 : 4}
        md={is600W ? 12 : 2}
        display="flex"
        justifyContent={is600W ? "space-between" : "center"}
        width={"100%"}
      >
        {is600W ? <Typography color="#4F4F4F">Price :</Typography> : null}
        <Typography align="center">{rupiah(price)}</Typography>
      </Grid>
    </Grid>
  );
};
