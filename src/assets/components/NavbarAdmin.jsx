import React, { useContext } from "react";
import logo from "../img/logo.png";
import { Link, useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import LogoutIcon from "@mui/icons-material/Logout";
import PersonIcon from "@mui/icons-material/Person";
import HorizontalRuleIcon from "@mui/icons-material/HorizontalRule";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import Divider from "@mui/material/Divider";
import { AuthContext } from "../../context/AuthProvider";

export const NavbarAdmin = () => {
  const { token, user, logout } = useContext(AuthContext);
  const navigate = useNavigate();
  const [isDrawerOpen, setIsDrawerOpen] = React.useState(false);

  const toggleDrawer = () => {
    setIsDrawerOpen(!isDrawerOpen);
  };
  const handleLogout = () => {
    logout(); // Memanggil fungsi logout dari AuthContext
    // Tambahkan logika lain yang diperlukan setelah logout,
  };

  return (
    <nav>
      <Grid
        position="fixed"
        top={0}
        zIndex={999}
        container
        justifyContent="space-between"
        alignItems="center"
        style={{
          backgroundColor: "#fff",
          padding: "20px",
          color: "#000",
        }}
      >
        <Grid item>
          <Link
            to="/"
            style={{
              textDecoration: "none",
              color: "black",
              display: "flex",
              alignItems: "center",
            }}
          >
            <img
              alt="logo"
              src={logo}
              style={{
                height: "30px",
                marginRight: "10px",
              }}
            />
            <Typography variant="h5" style={{ margin: 0 }}>
              Otomobil
            </Typography>
          </Link>
        </Grid>
        <Grid item>
          <Grid container spacing={4} alignItems="center">
            <Grid
              item
              sx={{ display: { xs: "none", md: "block", cursor: "pointer" } }}
              onClick={() => navigate("/checkout")}
            >
              <ShoppingCartIcon style={{ color: "#790B0A" }} />
            </Grid>
            <Grid
              onClick={() => navigate("/myclass")}
              item
              sx={{ display: { xs: "none", md: "block" } }}
            >
              <Typography
                variant="h6"
                style={{ color: "#790B0A", cursor: "pointer" }}
              >
                My Class
              </Typography>
            </Grid>
            <Grid
              item
              sx={{ display: { xs: "none", md: "block", cursor: "pointer" } }}
              onClick={() => navigate("/invoice")}
            >
              <Typography
                variant="h6"
                style={{ color: "#790B0A", cursor: "pointer" }}
              >
                Invoice
              </Typography>
            </Grid>
            <Grid item sx={{ display: { xs: "none", md: "block" } }}>
              <HorizontalRuleIcon
                style={{ transform: "rotate(90deg)", fontSize: "2rem" }}
              />
            </Grid>
            <Grid
              item
              sx={{ display: { xs: "none", md: "block" }, cursor: "pointer" }}
              onClick={() => navigate("/adminpanel")}
            >
              <PersonIcon style={{ color: "#790B0A" }} />
            </Grid>
            <Grid
              item
              sx={{ display: { xs: "none", md: "block" }, cursor: "pointer" }}
            >
              <LogoutIcon onClick={handleLogout} />
            </Grid>
            <Grid item sx={{ display: { md: "none" } }}>
              <IconButton
                color="inherit"
                aria-label="menu"
                onClick={toggleDrawer}
              >
                <MenuIcon />
              </IconButton>
              <Drawer anchor="right" open={isDrawerOpen} onClose={toggleDrawer}>
                <List
                  sx={{ width: 200, cursor: "pointer" }}
                  onClick={toggleDrawer}
                >
                  <ListItem
                    style={{ cursor: "pointer" }}
                    onClick={() => navigate("/checkout")}
                  >
                    <ShoppingCartIcon style={{ color: "#790B0A" }} />
                  </ListItem>
                  <ListItem
                    style={{ cursor: "pointer" }}
                    onClick={() => navigate("/myclass")}
                  >
                    <Typography variant="h6" style={{ color: "#790B0A" }}>
                      My Class
                    </Typography>
                  </ListItem>
                  <ListItem
                    style={{ cursor: "pointer" }}
                    onClick={() => navigate("/invoice")}
                  >
                    <Typography variant="h6" style={{ color: "#790B0A" }}>
                      Invoice
                    </Typography>
                  </ListItem>
                  <Divider sx={{ backgroundColor: "grey" }} />
                  <ListItem
                    style={{ cursor: "pointer" }}
                    onClick={() => navigate("/adminpanel")}
                  >
                    <PersonIcon style={{ color: "#790B0A" }} />
                  </ListItem>
                  <ListItem
                    style={{ cursor: "pointer" }}
                    onClick={handleLogout}
                  >
                    <LogoutIcon />
                  </ListItem>
                </List>
              </Drawer>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </nav>
  );
};
