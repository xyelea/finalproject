import * as React from "react";
import Avatar from "@mui/material/Avatar";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import { Grid } from "@mui/material";
import ButtonComponent from "./Button";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export const PayDialog = (props) => {
  const apilink = process.env.REACT_APP_BASE_URL;
  const {
    onClose,
    selectedValue,
    setSelectedValue,
    open,
    payments,
    items,
    checkedItems,
  } = props;

  const navigate = useNavigate();

  const handleClose = () => {
    onClose();
  };

  const handleListItemClick = (event, payment) => {
    setSelectedValue(payment.idPaymentMethod);
  };

  const payHandler = () => {
    const checker = [];
    for (let i in items) {
      for (let k in checkedItems) {
        if (items[i].id == k && checkedItems[k]) {
          checker.push(items[i]);
          break;
        }
      }
    }

    if (!checker.length) {
      console.log("empty");
    } else {
      const data = {
        userId: checker[0].idUser,
        paymentId: selectedValue,
        cartId: checker.map((cartCourse) => cartCourse.id),
      };
      console.log(data);
      axios({
        method: "POST",
        url: `${apilink}/Invoice`,
        data: data,
      })
        .then((result) => {
          console.log(result.data);
        })
        .catch((err) => {
          console.log(err);
        });
      navigate("/success-purchase");
    }
  };

  return (
    <Dialog onClose={handleClose} open={open} maxWidth="xs" fullWidth>
      <DialogTitle textAlign={"center"}>Select Payment Method</DialogTitle>
      <List sx={{ pt: 0 }}>
        {payments.map((payment) => (
          <ListItem disableGutters key={payment.idPaymentMethod}>
            <ListItemButton
              selected={selectedValue === payment.idPaymentMethod}
              onClick={(event) => handleListItemClick(event, payment)}
              disabled={!payment.isActive}
            >
              <ListItemAvatar>
                <Avatar src={payment.image} />
              </ListItemAvatar>
              <ListItemText
                primary={payment.name}
                primaryTypographyProps={{
                  fontFamily: "Poppins",
                  fontSize: "18px",
                  fontWeight: "500",
                  color: "#41454D",
                }}
              />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      <Grid container spacing={0} sx={{ mb: "20px " }}>
        <Grid
          item
          md={6}
          sm={6}
          xs={6}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <ButtonComponent
            onClick={handleClose}
            text={"cancel"}
            variant="outlined"
          ></ButtonComponent>
        </Grid>
        <Grid
          item
          md={6}
          sm={6}
          xs={6}
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <ButtonComponent
            // onClick={()=>navigate('/success-purchase')}
            onClick={() => payHandler()}
            to="/success-purchase"
            text={"pay now"}
            variant="contained"
          ></ButtonComponent>
        </Grid>
      </Grid>
    </Dialog>
  );
};
