import React from "react";
import Button from "@mui/material/Button";
import { Link, useNavigate } from "react-router-dom";

const contained = {
  backgroundColor: "#790B0A",
  "&:hover": {
    backgroundColor: "#790B0A",
  },
  fontFamily: "montserrat",
};

const onlytext = {
  color: "#790B0A",
};

const outlined = {
  color: "#790B0A",
  border: "1px solid #790B0A",
  "&:hover": {
    border: "1px solid #790B0A",
  },
  fontFamily: "montserrat",
};

const ButtonComponent = ({
  text,
  variant,
  onClick,
  to = "/",
  startIcon,
  cStyle = {},
  disabled,
}) => {
  const navigate = useNavigate();

  const styles =
    variant === "contained"
      ? contained
      : variant === "outlined"
      ? outlined
      : onlytext;

  const handleClick = () => {
    if (onClick) {
      onClick();
    } else {
      navigate(to);
    }
  };

  if (onClick) {
    return (
      <Button
        disabled={disabled}
        variant={variant}
        sx={{ ...styles, ...cStyle }}
        onClick={handleClick}
      >
        {text}
      </Button>
    );
  }

  return (
    <Link to={to}>
      <Button
        variant={variant}
        sx={{ ...styles, ...cStyle }}
        startIcon={startIcon}
      >
        {text}
      </Button>
    </Link>
  );
};

export default ButtonComponent;
