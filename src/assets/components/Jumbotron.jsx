import React, { useEffect, useState } from "react";
import jumbotronImage from "../img/jumbotron-v2.png";
import { Box, Grid, Typography } from "@mui/material";

const Jumbotron = () => {
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);
  const [is600H, setIs600H] = useState(window.innerHeight <= 600);
  const [is900H, setIs900H] = useState(window.innerHeight <= 900);

  useEffect(() => {
    const handleResize = () => {
      setIs600W(window.innerWidth <= 712);
      setIs600H(window.innerHeight <= 600);
      setIs900H(window.innerHeight <= 900);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div style={{ position: "relative" }}>
      <img
        alt="logo"
        src={jumbotronImage}
        style={{
          width: "100%",
          maxWidth: "100%",
          height: is600H ? "1200px" : is900H ? "900px" : "800px",
          objectFit: "cover",
        }}
      />
      <Box
        style={{
          position: "absolute",
          top: is600H || is600W ? "52%" : is900H || is600W ? "53%" : "48%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          textAlign: "center",
          width: "100%",
        }}
      >
        <Grid item xs={10}>
          <Typography
            color="white"
            sx={{ fontSize: { md: "3rem", sm: "3rem", xs: "2rem" } }}
          >
            We provide driving lessons for various types of cars
          </Typography>
          <Typography
            color="white"
            marginTop="50px"
            sx={{ fontSize: { md: "2rem", sm: "2rem", xs: "1.5rem" } }}
          >
            Professional staff who are ready to help you become a much-needed
            reliable driver
          </Typography>
        </Grid>
        <Grid
          container
          spacing={2}
          flexWrap={"wrap"}
          sx={{ marginTop: is600W ? "10px" : "180px" }}
          justifyContent="center"
        >
          <Grid item md={4}>
            <Typography
              color="white"
              sx={{ fontSize: { md: "3rem", sm: "3rem", xs: "2rem" } }}
            >
              50+
            </Typography>
            <Typography
              color="white"
              marginTop="30px"
              sx={{ fontSize: { md: "1.5rem", sm: "1.5rem", xs: "1rem" } }}
            >
              A class ready to make you a reliable <br />
              driver
            </Typography>
          </Grid>
          <Grid item md={4}>
            <Typography
              variant="h3"
              component="p"
              color="white"
              sx={{ fontSize: { md: "3rem", sm: "3rem", xs: "2rem" } }}
            >
              20+
            </Typography>
            <Typography
              color="white"
              marginTop="30px"
              sx={{ fontSize: { md: "1.5rem", sm: "1.5rem", xs: "1rem" } }}
            >
              Professional workforce with great
              <br />
              experience
            </Typography>
          </Grid>
          <Grid item md={4}>
            <Typography
              variant="h3"
              component="p"
              color="white"
              sx={{ fontSize: { md: "3rem", sm: "3rem", xs: "2rem" } }}
            >
              10+
            </Typography>
            <Typography
              color="white"
              marginTop="30px"
              sx={{ fontSize: { md: "1.5rem", sm: "1.5rem", xs: "1rem" } }}
            >
              Cooperate with driver service <br />
              partners
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};

export default Jumbotron;
