import React, { useEffect, useState } from "react";
import {
  Box,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
} from "@mui/material";
import ikon from "../img/iconKontak.svg";
import ikon2 from "../img/ikon-2.svg";
import ikon3 from "../img/ikon-3.svg";
import ikon4 from "../img/ikon-4.svg";
import ikon5 from "../img/ikon-5.svg";
import Divider from "@mui/material/Divider";
import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";
import axios from "axios";

export default function Footer() {
  const [product, setProduct] = useState([]);
  const apilink = process.env.REACT_APP_BASE_URL;

  const getProduct = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/Category`,
      headers: {
        "Content-Type": "application/json",
      },
    };

    axios
      .request(config)
      .then((response) => {
        setProduct(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getProduct();
  }, []);

  return (
    <div>
      <Divider />
      <Box
        sx={{
          backgroundColor: "#FFFFFF",
          padding: "40px",
          marginLeft: "auto",
          marginRight: "auto",
        }}
      >
        <Grid container spacing={4} justifyContent="center">
          <Grid item xs={12} sm={4}>
            {/* Column About Us */}
            <Typography variant="h6" color="#790B0A" mb={2}>
              About Us
            </Typography>
            <Typography variant="body2" color="textSecondary">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis et quasi architecto beatae vitae
              dicta sunt explicabo.
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4}>
            {/* Column Product */}
            <Typography variant="h6" color="#790B0A" marginLeft={"8px"}>
              Product
            </Typography>
            <Grid container sx={{ ml: "10px", mt: "10px" }}>
              {product.map((val) => {
                return (
                  <Grid key={val.id} item xs={6}>
                    <li>{val.name}</li>
                  </Grid>
                );
              })}
            </Grid>
          </Grid>
          <Grid item xs={12} sm={4}>
            {/* Column Address */}
            <Typography variant="h6" color="#790B0A" mb={2}>
              Address
            </Typography>
            <Typography variant="body2">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque.
            </Typography>
            <Typography variant="h6" color="#790B0A" mb={2} mt={2}>
              Contact Us
            </Typography>
            <Grid container spacing={2} alignItems="center">
              <Grid item xs={2}>
                <a href="https://www.facebook.com">
                  <img src={ikon} alt="" />
                </a>
              </Grid>
              <Grid item xs={2}>
                <a href="https://www.twitter.com">
                  <img src={ikon2} alt="" />
                </a>
              </Grid>
              <Grid item xs={2}>
                <a href="https://www.instagram.com">
                  <img src={ikon3} alt="" />
                </a>
              </Grid>
              <Grid item xs={2}>
                <a href="https://www.example.com">
                  <img src={ikon4} alt="" />
                </a>
              </Grid>
              <Grid item xs={2}>
                <a href="https://www.example.com">
                  <img src={ikon5} alt="" />
                </a>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
}
