import React, { useState } from "react";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
export const UserDialogAdd = ({
  openDialogAdd,
  handleCancel,
  name,
  email,
  password,
  handleNameChange,
  handleEmailChange,
  handlePasswordChange,
  handleSave,
  nameError,
  emailError,
  passwordError,
  emailErrorMessage,
  nameErrorMessage,
}) => {
  const [showPassword, setShowPassword] = useState(false);

  const handleTogglePassword = () => {
    setShowPassword(!showPassword);
  };

  return (
    <>
      {/* Dialog untuk Add button pada UserManagement */}
      <Dialog open={openDialogAdd}>
        <DialogTitle display="flex" justifyContent={"center"}>
          Add User
        </DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="Email"
            type="text"
            fullWidth
            value={email}
            onChange={handleEmailChange}
            helperText={emailError && emailErrorMessage}
            error={emailError}
          />
          <TextField
            margin="dense"
            label="Name"
            type="text"
            fullWidth
            value={name}
            onChange={handleNameChange}
            error={nameError}
            helperText={nameError && nameErrorMessage}
          />
          <TextField
            margin="dense"
            label="Password"
            fullWidth
            value={password}
            onChange={handlePasswordChange}
            error={passwordError}
            helperText={passwordError && "Password cannot be empty"}
            type={showPassword ? "text" : "password"}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    onClick={handleTogglePassword}
                    onMouseDown={(e) => e.preventDefault()}
                    edge="end">
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCancel} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSave} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export const UserDialogEdit = ({
  openDialogEdit,
  handleCancel,
  name,
  email,
  password,
  handleNameChange,
  handleEmailChange,
  handlePasswordChange,
  nameError,
  emailError,
  passwordError,
  role,
  status,
  handleRoleChange,
  handleStatusChange,
  handleSaveEdit,
  emailErrorMessage,
  nameErrorMessage,
}) => {
  return (
    <>
      {/* Dialogbox untuk edit button pada UserManagement */}
      <Dialog open={openDialogEdit}>
        <DialogTitle>Edit User</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="Email"
            type="text"
            fullWidth
            value={email}
            onChange={handleEmailChange}
            error={emailError}
            helperText={emailError && emailErrorMessage}
          />
          <TextField
            margin="dense"
            label="Name"
            type="text"
            fullWidth
            value={name}
            onChange={handleNameChange}
            error={nameError}
            helperText={nameError && nameErrorMessage}
          />
          <TextField
            margin="dense"
            label="Password"
            type="password"
            fullWidth
            value={password}
            onChange={handlePasswordChange}
            error={passwordError}
            helperText={passwordError && "Password cannot be empty"}
            disabled={true}
          />
          <Box sx={{ minWidth: 120, mt: "10px" }}>
            <FormControl fullWidth>
              <InputLabel id="role-label">Role</InputLabel>
              <Select
                labelId="role-label"
                id="role-select"
                value={role}
                onChange={handleRoleChange}>
                <MenuItem value={"admin"}>Admin</MenuItem>
                <MenuItem value={"member"}>Member</MenuItem>
              </Select>
            </FormControl>
          </Box>
          <Box sx={{ minWidth: 120, mt: "10px" }}>
            <FormControl fullWidth>
              <InputLabel id="status-label">Status</InputLabel>
              <Select
                labelId="status-label"
                id="status-select"
                value={status}
                onChange={handleStatusChange}>
                <MenuItem value={true}>Active</MenuItem>
                <MenuItem value={false}>Inactive</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCancel} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSaveEdit} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export const UserDialogPayAdd = ({
  openPaymentAdd,
  handleCancel,
  name,
  image,
  status,
  handleNameChange,
  handleImageChange,
  handleStatusChange,
  nameError,
  imageError,
  handleSave,
}) => {
  return (
    <>
      {/* Dialogbox untuk Add button pada Payment Method */}
      <Dialog open={openPaymentAdd}>
        <DialogTitle display="flex" justifyContent={"center"}>
          Add Payment Method
        </DialogTitle>
        <DialogContent>
          <TextField
            margin="dense"
            label="Name"
            type="text"
            fullWidth
            value={name}
            onChange={handleNameChange}
            error={nameError}
            helperText={nameError && "Name cannot be empty"}
          />
          <TextField
            margin="dense"
            label="Image"
            type="text"
            fullWidth
            value={image}
            onChange={handleImageChange}
            error={imageError}
            helperText={imageError && "Image cannot be empty"}
          />
          <Box sx={{ minWidth: 120, mt: "10px" }}>
            <FormControl fullWidth>
              <InputLabel id="status-label">Status</InputLabel>
              <Select
                labelId="status-label"
                id="status-select"
                value={status}
                onChange={handleStatusChange}>
                <MenuItem value={true}>Enabled</MenuItem>
                <MenuItem value={false}>Disabled</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCancel} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSave} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export const UserDialogPayEdit = ({
  openPaymentEdit,
  handleCancel,
  name,
  image,
  status,
  handleNameChange,
  handleImageChange,
  handleStatusChange,
  nameError,
  imageError,
  handleSaveEdit,
}) => {
  return (
    <>
      {/* Dialogbox untuk Edit button pada Payment Method */}
      <Dialog open={openPaymentEdit}>
        <DialogTitle display="flex" justifyContent={"center"}>
          Add Payment Method
        </DialogTitle>
        <DialogContent>
          <TextField
            margin="dense"
            label="Name"
            type="text"
            fullWidth
            value={name}
            onChange={handleNameChange}
            error={nameError}
            helperText={nameError && "Name cannot be empty"}
          />
          <TextField
            margin="dense"
            label="Image"
            type="text"
            fullWidth
            value={image}
            onChange={handleImageChange}
            error={imageError}
            helperText={imageError && "Name cannot be empty"}
          />
          <Box sx={{ minWidth: 120, mt: "10px" }}>
            <FormControl fullWidth>
              <InputLabel id="status-label">Status</InputLabel>
              <Select
                labelId="status-label"
                id="status-select"
                value={status}
                onChange={handleStatusChange}>
                <MenuItem value={true}>Enabled</MenuItem>
                <MenuItem value={false}>Disabled</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCancel} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSaveEdit} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

UserDialogAdd.defaultProps = { openDialogAdd: false };
UserDialogEdit.defaultProps = { openDialogEdit: false };
UserDialogPayAdd.defaultProps = { openPaymentAdd: false };
UserDialogPayEdit.defaultProps = { openPaymentEdit: false };
