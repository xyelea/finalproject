import React, { useEffect, useState } from "react";
import { Box, Typography } from "@mui/material";
import bannerImage from "../../img/listkelas-jumbotron.png";
import { useParams } from "react-router-dom";

const Banner = ({ item }) => {
  return (
    <>
      <Box>
        {" "}
        <img
          src={bannerImage}
          alt="Banner"
          style={{ width: "100%", marginTop: "75px" }}
        />
      </Box>

      <Box margin={"70px"}>
        <Typography variant="h4" component="h1" align="left">
          {item.name}
        </Typography>
        <Typography
          variant="body1"
          align="left"
          marginTop={"16px"}
          textAlign={"justify"}>
          {item.description}
        </Typography>
      </Box>
    </>
  );
};

export default Banner;
