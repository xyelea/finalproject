import AccountBoxIcon from "@mui/icons-material/AccountBox";
import DashboardIcon from "@mui/icons-material/Dashboard";
import LogoutIcon from "@mui/icons-material/Logout";
import PaymentsIcon from "@mui/icons-material/Payments";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import DescriptionIcon from "@mui/icons-material/Description";
import { Link, useNavigate } from "react-router-dom";
import { useContext, React } from "react";
import { AuthContext } from "../../context/AuthProvider";
import { List } from "@mui/material";

export const SecondaryListItems = () => {
  const navigate = useNavigate();
  const { token, user, logout } = useContext(AuthContext);
  const handleLogout = () => {
    logout(); // Memanggil fungsi logout dari AuthContext
    // Tambahkan logika lain yang diperlukan setelah logout,
  };
  return (
    <List>
      <ListSubheader component="div" inset>
        Action Menu
      </ListSubheader>
      {/* <ListItemButton component={Link} to="/adminpanel/sign-out"> */}
      <ListItemButton component={Link} onClick={handleLogout} to="/">
        <ListItemIcon>
          <LogoutIcon />
        </ListItemIcon>
        <ListItemText primary="Sign Out" />
      </ListItemButton>
    </List>
  );
};
