import React from "react";
import logo from "../img/logo.png";
import ButtonComponent from "./Button";
import { Link } from "react-router-dom";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";

export const Navbar = () => {
  const [isDrawerOpen, setIsDrawerOpen] = React.useState(false);

  const toggleDrawer = () => {
    setIsDrawerOpen(!isDrawerOpen);
  };

  return (
    <nav>
      <Grid
        position="absolute"
        top={0}
        zIndex={999}
        container
        justifyContent="space-between"
        alignItems="center"
        style={{
          backgroundColor: "#fff",
          padding: "20px",
          color: "#000",
        }}>
        <Grid item>
          <Link
            to="/"
            style={{
              textDecoration: "none",
              color: "black",
              display: "flex",
              alignItems: "center",
            }}>
            <img
              alt="logo"
              src={logo}
              style={{
                height: "30px",
                marginRight: "10px",
              }}
            />
            <Typography variant="h5" style={{ margin: 0 }}>
              Otomobil
            </Typography>
          </Link>
        </Grid>
        <Grid item>
          <Grid container spacing={1} alignItems="center">
            <Grid item sx={{ display: { xs: "none", md: "block" } }}>
              <ButtonComponent to="/signup" text="Sign Up" variant="text" />
            </Grid>
            <Grid item sx={{ display: { xs: "none", md: "block" } }}>
              <ButtonComponent to="/login" text="Login" variant="contained" />
            </Grid>
            <Grid item sx={{ display: { md: "none" } }}>
              <IconButton
                color="inherit"
                aria-label="menu"
                onClick={toggleDrawer}>
                <MenuIcon />
              </IconButton>
              <Drawer anchor="right" open={isDrawerOpen} onClose={toggleDrawer}>
                <List sx={{ width: 200 }} onClick={toggleDrawer}>
                  <ListItem>
                    <ButtonComponent
                      to="/signup"
                      text="Sign Up"
                      variant="text"
                    />
                  </ListItem>
                  <ListItem>
                    <ButtonComponent
                      to="/login"
                      text="Login"
                      variant="contained"
                    />
                  </ListItem>
                </List>
              </Drawer>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </nav>
  );
};
