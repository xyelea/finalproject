import React from "react";
import { Box, Pagination, Stack } from "@mui/material";

const FeatureSection = ({ children }) => {
  return (
    <Box
      sx={{ padding: "80px", backgroundColor: "#fff", marginBottom: "103px" }}>
      {/* Konten bagian tengah */}
      {children}
    </Box>
  );
};

export default FeatureSection;
