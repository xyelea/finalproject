import React, { useContext, useEffect, useState } from "react";
import logo from "../img/logo.png";
import { Link, useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import LogoutIcon from "@mui/icons-material/Logout";
import PersonIcon from "@mui/icons-material/Person";
import HorizontalRuleIcon from "@mui/icons-material/HorizontalRule";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import Divider from "@mui/material/Divider";
import { AuthContext } from "../../context/AuthProvider";
import { Badge } from "@mui/material";
import axios from "axios";

export const NavbarLogin = () => {
  const { token, user, logout } = useContext(AuthContext);
  const apilink = process.env.REACT_APP_BASE_URL;
  const userId = user.sub;
  const navigate = useNavigate();
  const [isDrawerOpen, setIsDrawerOpen] = React.useState(false);
  const [notif, setNotif] = useState([]);

  const fetchData = async () => {
    try {
      const response = await axios.get(`${apilink}/Cart?userId=${userId}`);
      setNotif(response.data);
    } catch (error) {
      console.log(error);
    }
  };
  const toggleDrawer = () => {
    setIsDrawerOpen(!isDrawerOpen);
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleLogout = () => {
    logout(); // Memanggil fungsi logout dari AuthContext
    // Tambahkan logika lain yang diperlukan setelah logout,
  };

  return (
    <nav>
      <Grid
        position="fixed"
        top={0}
        zIndex={999}
        container
        justifyContent="space-between"
        alignItems="center"
        style={{
          backgroundColor: "#fff",
          padding: "20px",
          color: "#000",
        }}
      >
        <Grid item>
          <Link
            to="/"
            style={{
              textDecoration: "none",
              color: "black",
              display: "flex",
              alignItems: "center",
            }}
          >
            <img
              alt="logo"
              src={logo}
              style={{
                height: "30px",
                marginRight: "10px",
              }}
            />
            <Typography variant="h5" style={{ margin: 0 }}>
              Otomobil
            </Typography>
          </Link>
        </Grid>
        <Grid item>
          <Grid container spacing={4} alignItems="center">
            <Grid
              item
              sx={{ display: { xs: "none", md: "block" }, cursor: "pointer" }}
              onClick={() => navigate("/checkout")}
            >
              <Badge
                badgeContent={notif.length}
                color="error"
                sx={{ main: "#790B0A" }}
              >
                <ShoppingCartIcon style={{ color: "#790B0A" }} />
              </Badge>
            </Grid>
            <Grid
              onClick={() => navigate("/myclass")}
              item
              sx={{ display: { xs: "none", md: "block" }, cursor: "pointer" }}
            >
              <Typography variant="h6" style={{ color: "#790B0A" }}>
                My Class
              </Typography>
            </Grid>
            <Grid
              item
              sx={{ display: { xs: "none", md: "block" }, cursor: "pointer" }}
            >
              <Typography
                variant="h6"
                style={{ color: "#790B0A" }}
                onClick={() => {
                  navigate("/invoice");
                }}
              >
                Invoice
              </Typography>
            </Grid>
            <Grid
              item
              sx={{ display: { xs: "none", md: "block" }, cursor: "pointer" }}
            >
              <HorizontalRuleIcon
                style={{ transform: "rotate(90deg)", fontSize: "2rem" }}
              />
            </Grid>

            <Grid
              item
              sx={{ display: { xs: "none", md: "block" }, cursor: "pointer" }}
            >
              <LogoutIcon onClick={handleLogout} />
            </Grid>
            <Grid item sx={{ display: { md: "none" } }}>
              <IconButton
                color="inherit"
                aria-label="menu"
                onClick={toggleDrawer}
              >
                <MenuIcon />
              </IconButton>
              <Drawer anchor="right" open={isDrawerOpen} onClose={toggleDrawer}>
                <List sx={{ width: 200 }} onClick={toggleDrawer}>
                  <ListItem
                    sx={{ cursor: "pointer" }}
                    onClick={() => {
                      navigate("/checkout");
                    }}
                  >
                    <ShoppingCartIcon style={{ color: "#790B0A" }} />
                  </ListItem>
                  <ListItem
                    sx={{ cursor: "pointer" }}
                    onClick={() => {
                      navigate("/myclass");
                    }}
                  >
                    <Typography variant="h6" style={{ color: "#790B0A" }}>
                      My Class
                    </Typography>
                  </ListItem>
                  <ListItem
                    sx={{ cursor: "pointer" }}
                    onClick={() => {
                      navigate("/invoice");
                    }}
                  >
                    <Typography variant="h6" style={{ color: "#790B0A" }}>
                      Invoice
                    </Typography>
                  </ListItem>
                  <Divider sx={{ backgroundColor: "grey" }} />
                  <ListItem onClick={handleLogout} sx={{ cursor: "pointer" }}>
                    <LogoutIcon />
                  </ListItem>
                </List>
              </Drawer>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </nav>
  );
};
