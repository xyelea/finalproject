import { Box, Typography } from "@mui/material";
import React from "react";

export default function CarSection() {
  return (
    <div>
      {" "}
      <Box sx={{ padding: "40px", backgroundColor: "#f5f5f5" }}>
        {/* Konten bagian tengah */}{" "}
        <Typography variant="h4" align="center" sx={{ marginBottom: "20px" }}>
          CarSection
        </Typography>
        {/* ... */}
      </Box>
    </div>
  );
}
