import React from "react";
import Snackbar from "@mui/material/Snackbar";
import SnackbarContent from "@mui/material/SnackbarContent";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import ErrorIcon from "@mui/icons-material/Error";

export const Notif = ({
  snackbarOpen,
  handleNotif,
  snackbarColor,
  snackbarMessage,
}) => {
  return (
    <Snackbar
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      open={snackbarOpen}
      autoHideDuration={3000}
      onClose={handleNotif}
    >
      <SnackbarContent
        style={{
          backgroundColor: snackbarColor,
          justifyContent: "center",
          alignItems: "center",
        }}
        message={
          <>
            {snackbarColor === "green" ? <CheckCircleIcon /> : <ErrorIcon />}
            <span>{snackbarMessage}</span>
          </>
        }
      />
    </Snackbar>
  );
};
