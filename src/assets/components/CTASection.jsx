import React, { useEffect, useState } from "react";
import bgCta from "../img/bgCta.jpg";
import AvatarCta from "../img/avatar.svg";
import { Box, Grid, Typography } from "@mui/material";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export default function CTASection() {
  const apilink = process.env.REACT_APP_BASE_URL;
  const navigate = useNavigate();
  const [category, setCategory] = useState([]);

  const getCategory = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/Category`,
      headers: {
        "Content-Type": "application/json",
      },
    };

    axios
      .request(config)
      .then((response) => {
        setCategory(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const renderCategory = () => {
    return category.map((val) => {
      return (
        <Grid
          item
          xs={6}
          sm={3}
          key={val.id}
          display={"flex"}
          justifyContent={"center"}
          alignItems={"center"}
        >
          <Box
            onClick={() => navigate(`/course-list/${val.name}`)}
            width={"100px"}
            sx={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <img
              src={val.image}
              alt={val.name}
              style={{ width: "75px", height: "50px" }}
            />
            <Typography variant="subtitle1" fontWeight="500" fontSize="24px">
              {val.name}
            </Typography>
          </Box>
        </Grid>
      );
    });
  };

  useEffect(() => {
    getCategory();
  }, []);

  return (
    <>
      <Box
        sx={{
          paddingLeft: "10%",
          backgroundColor: "white",
          marginTop: "10%",
          marginBottom: "5%",
        }}
      >
        <Grid container spacing={2}>
          <Grid item xs={12} md={7}>
            {/* Bagian kiri: Paragraf */}
            <Box textAlign="justify">
              <Typography
                color="#790B0A"
                fontWeight="bold"
                sx={{
                  fontSize: { md: "3rem", sm: "3rem", xs: "2rem" },
                  mr: { md: "0px", sm: "0px", xs: "40px" },
                }}
              >
                Gets your best benefit
              </Typography>
              <Typography
                variant="body1"
                marginTop="24px"
                fontSize="16px"
                sx={{ mr: { md: "0px", sm: "0px", xs: "40px" } }}
              >
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae
                vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi
                nesciunt.
              </Typography>
              <Typography
                mt={9}
                fontSize="16px"
                sx={{ mr: { md: "0px", sm: "0px", xs: "40px" } }}
              >
                Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                consectetur, adipisci velit, sed quia non numquam. Neque porro
                quisquam est, qui dolorem ipsum quia dolor sit amet,
                consectetur, adipisci velit, sed quia non numquam eius modi
                tempora incidunt ut labore et dolore magnam aliquam quaerat
                voluptatem.
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12} md={5}>
            {/* Bagian kanan: Gambar tumpang tindih */}

            <Box sx={{ position: "relative" }}>
              <img
                src={bgCta}
                alt="Gambar Tumpang Tindih 1"
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  width: "100%",
                  marginTop: "20%",
                }}
              />
              <img
                src={AvatarCta}
                alt="Gambar Tumpang Tindih 2"
                style={{
                  position: "absolute",
                  top: "50%",
                  bottom: 0,
                  width: "82%",
                }}
              />
            </Box>
          </Grid>
        </Grid>
      </Box>

      <Box
        sx={{
          textAlign: "center",
          margin: "0px 10%",
          padding: "80px 40px",
          mb: "100px",
        }}
      >
        <Typography
          color="#790B0A"
          fontWeight="bold"
          mb={10}
          mt={20}
          sx={{ fontSize: { md: "2rem", sm: "2rem", xs: "1.5rem" } }}
        >
          More car types you can choose
        </Typography>
        <Grid
          container
          columnSpacing={1}
          rowSpacing={6}
          justifyContent="center"
          alignItems="center"
        >
          {renderCategory()}
        </Grid>
      </Box>
    </>
  );
}
