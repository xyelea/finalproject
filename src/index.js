import React, { useContext } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import { ChangePass } from "./pages/ChangePass";
import { Landingpage } from "./pages/Landingpage";
import { Login } from "./pages/Login";
import { ResetPass } from "./pages/ResetPass";
import { SignUp } from "./pages/SignUp";
import { LandingPageLogin } from "./pages/LandingPageRegister";
import { ListMenuKelas } from "./pages/ListMenuKelas";
import { EmailConfirmSuccess } from "./pages/EmailConfirmSuccess";
import { SuccessPurchase } from "./pages/SuccessPurchase";
import { CheckoutPage } from "./pages/CheckoutPage";
import DetailKelas from "./pages/DetailKelas";
import { Invoice } from "./pages/Invoice";
import { DetailInvoice } from "./pages/DetailInvoice";
import AdminPanel from "./pages/AdminPanel";
import UserManagement from "./pages/UserManagement.jsx";
import PaymentMethod from "./pages/PaymentMethod";
import InvoicePanel from "./pages/InvoicePanel";
import { MyClass } from "./pages/MyClass";
import { AuthProvider } from "./context/AuthProvider";
import ProtectedRoute from "./assets/components/ProtectedRoute";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <AuthProvider>
        <Routes>
          <Route path="/" element={<Landingpage />} />
          <Route path="/landingpage" element={<Landingpage />} />
          <Route path="/login" element={<Login />} />
          <Route path="/signup" element={<SignUp />} />
          <Route path="/resetpass" element={<ResetPass />} />
          <Route path="/changepass" element={<ChangePass />} />
          <Route path="/email-conf" element={<EmailConfirmSuccess />} />
          <Route path="/course-detail/:id" element={<DetailKelas />} />
          <Route path="/course-list/:name" element={<ListMenuKelas />} />
          <Route
            path="/checkout"
            element={
              <ProtectedRoute
                element={<CheckoutPage />}
                roles={["member", "admin"]}
                redirectTo={"/"}
              />
            }
          />
          <Route
            path="/invoice"
            element={
              <ProtectedRoute
                element={<Invoice />}
                roles={["member", "admin"]}
                redirectTo={"/"}
              />
            }
          />
          <Route
            path="/checkout"
            element={
              <ProtectedRoute
                element={<CheckoutPage />}
                roles={["member", "admin"]}
                redirectTo={"/"}
              />
            }
          />
          <Route
            path="/detail-invoice/:id"
            element={
              <ProtectedRoute
                element={<DetailInvoice />}
                roles={["member", "admin"]}
                redirectTo={"/"}
              />
            }
          />
          <Route
            path="/success-purchase"
            element={
              <ProtectedRoute
                element={<SuccessPurchase />}
                roles={["member", "admin"]}
                redirectTo={"/"}
              />
            }
          />
          <Route
            path="/invoice"
            element={
              <ProtectedRoute
                element={<Invoice />}
                roles={["member", "admin"]}
                redirectTo={"/"}
              />
            }
          />
          <Route
            path="/detail-invoice/:id"
            element={
              <ProtectedRoute
                element={<DetailInvoice />}
                roles={["member", "admin"]}
                redirectTo={"/"}
              />
            }
          />

          <Route
            path="/adminpanel/user-management"
            element={
              <ProtectedRoute
                element={<UserManagement />}
                roles={["admin"]}
                redirectTo="/"
              />
            }
          />
          <Route
            path="/adminpanel/payment-method"
            element={
              <ProtectedRoute
                element={<PaymentMethod />}
                roles={["admin"]}
                redirectTo="/"
              />
            }
          />
          <Route
            path="/adminpanel/invoice-users"
            element={
              <ProtectedRoute
                element={<InvoicePanel />}
                roles={["admin"]}
                redirectTo="/"
              />
            }
          />

          <Route
            path="/myclass"
            element={
              <ProtectedRoute
                element={<MyClass />}
                roles={["member", "admin"]}
                redirectTo="/login"
              />
            }
          />
          <Route
            path="/adminpanel"
            element={
              <ProtectedRoute
                element={<AdminPanel />}
                roles={["admin"]}
                redirectTo="/"
              />
            }
          />
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  </React.StrictMode>
);
