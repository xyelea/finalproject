import React, { useEffect, useState } from "react";
import {
  Alert,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
} from "@mui/material";
import { Navbar } from "../assets/components/Navbar";
import TextFieldComponent from "../assets/components/TextField";
import ButtonComponent from "../assets/components/Button";
import { Link, useNavigate } from "react-router-dom";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import axios from "axios";
import { Visibility, VisibilityOff } from "@mui/icons-material";

export const SignUp = () => {
  const apilink = process.env.REACT_APP_BASE_URL;
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const navigate = useNavigate();
  const [is600H, setIs600H] = useState(window.innerHeight <= 600);
  const [is900H, setIs900H] = useState(window.innerHeight <= 900);

  const checkIfEmailExists = (email) => {
    const existingEmails = [
      "dummy@example.com",
      "user1@example.com",
      "user2@example.com",
      "user3@example.com",
      "tama@example.com",
    ];

    return existingEmails.includes(email);
  };

  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleClickShowConfirmPassword = () =>
    setShowConfirmPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleSignUp = async () => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

    if (
      name === "" ||
      email === "" ||
      password === "" ||
      confirmPassword === ""
    ) {
      setError("Please fill in all fields.");
    } else if (!emailRegex.test(email)) {
      setError("Invalid email. Please enter a valid email address.");
    } else if (!passwordRegex.test(password)) {
      setError(
        "Invalid password. Password should contain at least 8 characters, including one letter and one number."
      );
    } else if (password !== confirmPassword) {
      setError("Password and confirm password do not match.");
    } else {
      const isEmailExists = checkIfEmailExists(email);
      if (isEmailExists) {
        setError("Email is already in use. Please choose a different email.");
      } else {
        try {
          const response = await axios.post(`${apilink}/user`, {
            username: name,
            password,
            role: "member",
            email,
          });
          // Reset form and navigate to login page
          setName("");
          setEmail("");
          setPassword("");
          setConfirmPassword("");
          setError("");
          navigate("/login");
        } catch (error) {
          if (error.response && error.response.data) {
            setError(error.response.data); // Set pesan error dari respons ke state error
          } else {
            setError("An error occurred"); // Pesan error default jika tidak ada respons atau data respons
          }
        }
      }
    }
  };

  useEffect(() => {
    const handleResize = () => {
      setIs600H(window.innerHeight <= 600);
      setIs900H(window.innerHeight <= 900);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div>
      <Navbar />
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        sx={{ paddingTop: is600H ? 10 : is900H ? 13 : 30 }}>
        <Grid item md={6} xs={8}>
          <Stack spacing={2} width="100%">
            <Typography
              variant="h4"
              sx={{
                color: "#790B0A",
                marginBottom: "10px",
                textAlign: "start",
              }}>
              Let's Join our course!
            </Typography>
            <Typography
              variant="body1"
              sx={{
                marginBottom: is600H ? "" : "20px",
                textAlign: "start",
              }}>
              Please register first
            </Typography>

            <TextFieldComponent
              label="Name"
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />

            <TextFieldComponent
              label="Email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />

            <FormControl variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={showPassword ? "text" : "password"}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end">
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormControl>

            <FormControl variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Confirm Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={showConfirmPassword ? "text" : "password"}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowConfirmPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end">
                      {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Confirm Password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
            </FormControl>
          </Stack>
          {error && (
            <Alert
              severity="error"
              sx={{ color: "red", marginTop: "5px", textAlign: "center" }}>
              {error}
            </Alert>
          )}
          <Grid item container justifyContent="end" pt={2}>
            <ButtonComponent
              text="Sign Up"
              variant="contained"
              onClick={handleSignUp}
            />
          </Grid>
          <Grid item container justifyContent="center" marginTop="20px">
            <Typography variant="body2">
              Have an account?{" "}
              <Link
                to="/login"
                style={{
                  textDecoration: "none",
                  color: "#2F80ED",
                  cursor: "pointer",
                }}>
                Login here
              </Link>
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};
