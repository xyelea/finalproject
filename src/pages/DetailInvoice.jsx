import { Grid, Typography } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import React, { useContext, useEffect, useState } from "react";
import Footer from "../assets/components/Footer";
import { NavbarLogin } from "../assets/components/NavbarLogin";
import { NavbarAdmin } from "../assets/components/NavbarAdmin";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import moment from "moment";
import { rupiah } from "../utility/formatIDR";
import { AuthContext } from "../context/AuthProvider";
import { InvoiceDetailCard } from "../assets/components/InvoiceCard";

export const DetailInvoice = () => {
  const { token, user } = useContext(AuthContext);
  const navigate = useNavigate();
  const apilink = process.env.REACT_APP_BASE_URL;
  const { id } = useParams();
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);
  const [detailInvoice, setDetailInvoice] = useState([]);
  const [courses, setCourses] = useState([]);

  const getDetailInvoice = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/Invoice/detail?InvoiceId=${id}`,
      headers: {},
    };

    axios
      .request(config)
      .then((response) => {
        setDetailInvoice(response.data);
        setCourses(response.data.courses);
      })
      .catch((error) => {});
  };
  useEffect(() => {
    const handleResize = () => {
      setIs600W(window.innerWidth <= 600);
    };

    getDetailInvoice();
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  const renderNavbar = () => {
    if (user && user.role === "member") {
      return <NavbarLogin />;
    } else if (user && user.role === "admin") {
      return <NavbarAdmin />;
    }
  };

  const renderCourse = () => {
    return courses.map((course, index) => (
      <InvoiceDetailCard
        key={index}
        no={index + 1}
        courseName={course.courseName}
        courseCategory={course.courseCategory}
        date={course.courseDate}
        price={course.coursePrice}
      />
    ));
  };

  return (
    <>
      <CssBaseline />
      <Grid container direction="column" minHeight="100vh">
        {renderNavbar()}
        <Grid
          item
          container
          justifyContent="center"
          alignItems="center"
          mt="121px"
          paddingLeft={is600W ? "10px" : "70px"}
          paddingRight={is600W ? "10px" : "70px"}
        >
          {/* {if(user.role ==="admin"){

          }} */}
          <Grid container direction="row">
            <Typography
              variant="h6"
              color="#828282"
              onClick={() => navigate("/")}
            >
              Home {">"}
            </Typography>
            <Typography
              variant="h6"
              color="#828282"
              marginLeft="10px"
              onClick={() => navigate(-1)}
            >
              Invoice {">"}
            </Typography>
            <Typography variant="h6" color="#790B0A" marginLeft="10px">
              Detail Invoice
            </Typography>
          </Grid>
          <Grid item container mt={is600W ? "10px" : "32px"}>
            <Typography variant={is600W ? "h5" : "h4"}>Menu Invoice</Typography>
          </Grid>
          <Grid
            container
            display="flex"
            direction="row"
            mt={is600W ? "" : "30px"}
          >
            <Typography variant={is600W ? "h6" : "h5"}>No. Invoice:</Typography>
            <Typography
              variant={is600W ? "h6" : "h5"}
              ml={is600W ? "5px" : "80px"}
            >
              {detailInvoice.id}
            </Typography>
          </Grid>
          <Grid
            container
            display="flex"
            direction="row"
            mt={is600W ? "" : "5px"}
          >
            <Typography variant={is600W ? "h6" : "h5"}>Date:</Typography>
            <Typography
              variant={is600W ? "h6" : "h5"}
              ml={is600W ? "50px" : "150px"}
            >
              {moment(detailInvoice.date).format("D MMMM YYYY")}
            </Typography>
            {is600W ? null : (
              <Typography variant={"h5"} ml="auto">
                Total Price {rupiah(detailInvoice.totalPrice)}
              </Typography>
            )}
          </Grid>
          {is600W ? (
            <Grid container display="flex" justifyContent={"start"}>
              <Typography variant="h6">
                Total Price {rupiah(detailInvoice.totalPrice)}
              </Typography>
            </Grid>
          ) : null}
          {is600W ? (
            <>{renderCourse()} </>
          ) : (
            <>
              <Grid
                item
                display="flex"
                justifyContent="space-between"
                paddingLeft="20px"
                paddingRight="20px"
                bgcolor="#790B0A"
                height="60px"
                alignItems="center"
                width="100%"
                mt="24px"
              >
                <Grid item xs={2} md={1}>
                  <Typography color="#fff">No</Typography>
                </Grid>
                <Grid item xs={4} md={10}>
                  <Typography color="#fff" align="center">
                    Course Name
                  </Typography>
                </Grid>
                <Grid item xs={4} md={2}>
                  <Typography color="#fff" align="center">
                    Type
                  </Typography>
                </Grid>
                <Grid item xs={4} md={2}>
                  <Typography color="#fff" align="center">
                    Schedule
                  </Typography>
                </Grid>
                <Grid item xs={4} md={2}>
                  <Typography color="#fff" align="center">
                    Price
                  </Typography>
                </Grid>
              </Grid>
              {renderCourse()}
            </>
          )}
        </Grid>
        <Grid item mt="auto">
          <Footer />
        </Grid>
      </Grid>
    </>
  );
};
