import React, { useContext, useEffect, useState } from "react";
import Checkbox from "@mui/material/Checkbox";
import { Box, Button, Container, Grid, Typography } from "@mui/material";
import { PayDialog } from "../assets/components/PayDialog";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import ButtonComponent from "../assets/components/Button";
import { NavbarLogin } from "../assets/components/NavbarLogin";
import { rupiah } from "../utility/formatIDR";
import axios from "axios";
import moment from "moment";
import { AuthContext } from "../context/AuthProvider";
import { NavbarAdmin } from "../assets/components/NavbarAdmin";
import { Navbar } from "../assets/components/Navbar";

export const CheckoutPage = () => {
  const apilink = process.env.REACT_APP_BASE_URL;
  const { token, user } = useContext(AuthContext);

  const userId = user.sub;

  const [open, setOpen] = useState(false);
  const [selectedValue, setSelectedValue] = useState();
  const [items, setItems] = useState([]);
  const [selectAll, setSelectAll] = useState(false);
  const [checkedItems, setCheckedItems] = useState({});
  const [payments, setPayments] = useState([]);

  const fetchData = async () => {
    try {
      const response = await axios.get(`${apilink}/paymentmethods`);
      setPayments(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleSelectAllChange = (event) => {
    const isChecked = event.target.checked;
    setSelectAll(isChecked);
    setCheckedItems(
      items.reduce((obj, item) => {
        obj[item.id] = isChecked;
        return obj;
      }, {})
    );
  };

  const handleChange = (event, itemId) => {
    const isChecked = event.target.checked;
    setCheckedItems((prevCheckedItems) => ({
      ...prevCheckedItems,
      [itemId]: isChecked,
    }));
    setSelectAll(
      Object.values({ ...checkedItems, [itemId]: isChecked }).every(
        (item) => item
      )
    );
  };

  const handleDelete = (itemId) => {
    let config = {
      method: "delete",
      maxBodyLength: Infinity,
      url: `${apilink}/Cart?userId=${userId}&Id=${itemId}`,
      headers: {},
    };

    axios
      .request(config)
      .then((response) => {
        setItems((prevItems) => prevItems.filter((item) => item.id !== itemId));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const calculateTotal = () => {
    let total = 0;
    items.forEach((item) => {
      let price = item.coursePrice;
      if (checkedItems[item.id]) {
        total += price;
      }
    });
    return total;
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const getItemCheckout = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/Cart?userId=${userId}`,
    };

    axios
      .request(config)
      .then((response) => {
        const fetchedItems = response.data;
        // Initialize the checkedItems object
        const initialCheckedItems = fetchedItems.reduce((obj, item) => {
          obj[item.id] = false;
          return obj;
        }, {});
        setItems(fetchedItems);
        setCheckedItems(initialCheckedItems);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const renderNavbar = () => {
    if (user && user.role === "member") {
      return <NavbarLogin />;
    } else if (user && user.role === "admin") {
      return <NavbarAdmin />;
    } else {
      return <Navbar />;
    }
  };
  useEffect(() => {
    getItemCheckout();
    fetchData();
  }, []);

  return (
    <Box sx={{ width: "100%" }}>
      {renderNavbar()}
      <Container sx={{ maxWidth: "1200px", minHeight: "100vh", mt: "75px" }}>
        <Box
          borderBottom={"1px solid #BDBDBD"}
          sx={{
            display: "flex",
            alignItems: "center",
            paddingBottom: "24px",
            paddingTop: "24px",
          }}
        >
          <Checkbox
            checked={selectAll}
            indeterminate={
              Object.values(checkedItems).some((item) => item) &&
              !Object.values(checkedItems).every((item) => item)
            }
            onChange={handleSelectAllChange}
          />
          <Typography
            sx={{
              fontFamily: "montserrat",
              color: "#333333",
              fontSize: "20px",
              fontWeight: "400",
            }}
          >
            Pilih Semua
          </Typography>
        </Box>

        {items.length !== 0 ? (
          items.map((item) => (
            <Box key={item.id}>
              <Grid
                container
                alignItems={"center"}
                spacing={2}
                sx={{
                  borderBottom: "1px solid #BDBDBD",
                  paddingTop: "24px",
                  paddingBottom: "24px",
                }}
              >
                <Grid item md={0.5} sm={1} xs={12}>
                  <Checkbox
                    checked={checkedItems[item.id]}
                    onChange={(event) => handleChange(event, item.id)}
                  />
                </Grid>
                <Grid item md={2} sm={3} xs={12}>
                  <img
                    alt={item.courseName}
                    src={item.courseImage}
                    width="100%"
                  ></img>
                </Grid>
                <Grid item md={8.5} sm={6.5} xs={10}>
                  <Box>
                    <Typography
                      sx={{
                        fontFamily: "montserrat",
                        color: "#828282",
                        fontSize: "16px",
                        fontWeight: "400",
                      }}
                    >
                      {item.courseCategory}
                    </Typography>
                    <Typography
                      sx={{
                        fontFamily: "montserrat",
                        color: "#333333",
                        fontSize: "24px",
                        fontWeight: "600",
                      }}
                    >
                      {item.courseName}
                    </Typography>
                    <Typography
                      sx={{
                        fontFamily: "montserrat",
                        color: "#4F4F4F",
                        fontSize: "16px",
                        fontWeight: "400",
                      }}
                    >
                      Schedule : {moment(item.date).format("dddd, D MMMM YYYY")}
                    </Typography>
                    <Typography
                      sx={{
                        fontFamily: "montserrat",
                        color: "#790B0A",
                        fontSize: "20px",
                        fontWeight: "600",
                      }}
                    >
                      {rupiah(item.coursePrice)}
                    </Typography>
                  </Box>
                </Grid>
                <Grid item md={1} sm={1} xs={2}>
                  <Button onClick={() => handleDelete(item.id)}>
                    <DeleteForeverIcon
                      sx={{
                        color: "#EB5757",
                        fontSize: "35px",
                      }}
                    ></DeleteForeverIcon>
                  </Button>
                </Grid>
              </Grid>
            </Box>
          ))
        ) : (
          <Box
            sx={{
              height: "400px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Typography
              sx={{
                fontFamily: "montserrat",
                color: "#333333",
                fontSize: "20px",
                fontWeight: "400",
              }}
            >
              No Course Selected
            </Typography>
          </Box>
        )}
      </Container>
      <Box
        marginTop={"auto"}
        sx={{
          borderTop: "1px solid #BDBDBD",
          paddingBottom: "30px",
          paddingTop: "30px",
          width: "100%",
          mt: "auto",
          bottom: "0",
          position: "sticky",
          backgroundColor: "white",
        }}
      >
        <Grid container spacing={2} alignItems={"center"}>
          <Grid item md={2} sm={2} xs={3} textAlign={"center"}>
            <Typography
              sx={{
                fontFamily: "montserrat",
                color: "#333333",
                fontSize: { md: "18px", sm: "18px", xs: "14px" },
                fontWeight: "400",
              }}
            >
              Total Price
            </Typography>
          </Grid>
          <Grid item md={8} sm={8} xs={5}>
            <Typography
              sx={{
                fontFamily: "montserrat",
                color: "#790B0A",
                fontSize: { md: "24px", sm: "24px", xs: "18px" },
                fontWeight: "600",
              }}
            >
              {rupiah(calculateTotal())}
            </Typography>
          </Grid>
          <Grid item md={2} sm={2} xs={4}>
            <ButtonComponent
              text="Pay Now"
              onClick={handleClickOpen}
              variant="contained"
              disabled={Object.values(checkedItems).every((item) => !item)}
            ></ButtonComponent>
            <PayDialog
              selectedValue={selectedValue}
              setSelectedValue={setSelectedValue}
              payments={payments}
              open={open}
              onClose={handleClose}
              items={items}
              checkedItems={checkedItems}
            />
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
