import { Box, Divider, Grid } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import CarCard from "../assets/components/CarCard";
import FeatureSection from "../assets/components/FeatureSection";
import Footer from "../assets/components/Footer";
import SectionTitle from "../assets/components/SectionTitle";
import Banner from "../assets/components/ListKelasUI/Banner";
import { Navbar } from "../assets/components/Navbar";
import { NavbarAdmin } from "../assets/components/NavbarAdmin";
import { NavbarLogin } from "../assets/components/NavbarLogin";
import { AuthContext } from "../context/AuthProvider";

export const ListMenuKelas = () => {
  const { token, user } = useContext(AuthContext);
  const apilink = process.env.REACT_APP_BASE_URL;
  const navigate = useNavigate();
  const { name } = useParams();
  const [item, setItem] = useState([]);
  const [course, setCourse] = useState([]);

  const getCourse = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/Course/getAllByCategory?name=${name}`,
      headers: {
        "Content-Type": "application/json",
      },
    };

    axios
      .request(config)
      .then((response) => {
        setCourse(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const renderCourse = () => {
    return course.map((val) => {
      return (
        <Grid key={val.id} item xs={12} sm={6} md={4}>
          <Box onClick={() => navigate(`/course-detail/${val.id}`)}>
            <CarCard
              image={val.image}
              carType={val.category}
              price={val.price}
              carName={val.name}
            />
          </Box>
        </Grid>
      );
    });
  };
  const renderNavbar = () => {
    if (user && user.role === "member") {
      return <NavbarLogin />;
    } else if (user && user.role === "admin") {
      return <NavbarAdmin />;
    } else {
      return <Navbar />;
    }
  };
  useEffect(() => {
    getCourse();
    window.scrollTo(0, 0);
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/Category/detail?name=${name}`,
      headers: {},
    };

    axios
      .request(config)
      .then((response) => {
        setItem(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [name]);
  return (
    <div>
      {/* Gunakan CssBaseline untuk menghilangkan margin dan padding secara default */}
      <CssBaseline />
      {renderNavbar()}
      <Banner item={item} />
      <Divider />
      <FeatureSection>
        <Grid item xs={12}>
          <SectionTitle text="Another favorite course" />
        </Grid>
        <Grid container flexWrap={"wrap"} spacing={2}>
          {renderCourse()}
        </Grid>
      </FeatureSection>
      {/* <CTASection /> */}
      <Footer />
    </div>
  );
};
