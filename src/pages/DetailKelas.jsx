import React, { useContext, useEffect, useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import FeatureSection from "../assets/components/FeatureSection";
import Footer from "../assets/components/Footer";
import CarCard from "../assets/components/CarCard";
import SectionTitle from "../assets/components/SectionTitle";
import { Box, Divider, Grid } from "@mui/material";
import { NavbarLogin } from "../assets/components/NavbarLogin";
import Jumbotron from "../assets/components/DetailKelasUI/Jumbotron";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import { AuthContext } from "../context/AuthProvider";
import { Navbar } from "../assets/components/Navbar";
import { NavbarAdmin } from "../assets/components/NavbarAdmin";

export default function DetailKelas() {
  const { token, user } = useContext(AuthContext);

  const apilink = process.env.REACT_APP_BASE_URL;
  const navigate = useNavigate();
  const { id } = useParams();
  const [courseDetail, setCourseDetail] = useState([]);
  const [course, setCourse] = useState([]);

  const getCourse = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/Course/getAllOther?id=${id}`,
      headers: {
        "Content-Type": "application/json",
      },
    };

    axios
      .request(config)
      .then((response) => {
        setCourse(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleNavigate = (id) => {
    navigate(`/course-detail/${id}`);
    window.location.reload();
  };

  const renderCourse = () => {
    return course.map((val) => {
      return (
        <Grid key={val.id} item xs={12} sm={6} md={4}>
          <Box onClick={() => handleNavigate(val.id)}>
            <CarCard
              image={val.image}
              carType={val.category}
              price={val.price}
              carName={val.name}
            />
          </Box>
        </Grid>
      );
    });
  };

  useEffect(() => {
    getCourse();
    // window.location.reload();
    window.scrollTo(0, 0);
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/Course/detail?id=${id}`,
      headers: {},
    };

    axios
      .request(config)
      .then((response) => {
        setCourseDetail(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  const renderNavbar = () => {
    if (user && user.role === "member") {
      return <NavbarLogin />;
    } else if (user && user.role === "admin") {
      return <NavbarAdmin />;
    } else {
      return <Navbar />;
    }
  };
  return (
    <div>
      {/* Gunakan CssBaseline untuk menghilangkan margin dan padding secara default */}
      <CssBaseline />
      {renderNavbar()}
      <Jumbotron value={courseDetail} user={user.sub} token={token} />
      <Divider />
      <FeatureSection>
        <Grid item xs={12}>
          <SectionTitle text="Another favorite course" />
        </Grid>
        <Grid container flexWrap={"wrap"} spacing={2}>
          {renderCourse()}
        </Grid>
      </FeatureSection>
      {/* <CTASection /> */}
      <Footer />
    </div>
  );
}
