import React from "react";
import { Navbar } from "../assets/components/Navbar";
import Jumbotron from "../assets/components/Jumbotron";
import CssBaseline from "@mui/material/CssBaseline";
import FeatureSection from "../assets/components/FeatureSection";
import Footer from "../assets/components/Footer";
import CarCard from "../assets/components/CarCard";
import SectionTitle from "../assets/components/SectionTitle";
import car1Image from "../assets/img/1.jpg";
import car2Image from "../assets/img/2.jpg";
import car3Image from "../assets/img/3.jpg";
import car4Image from "../assets/img/4.jpg";
import car5Image from "../assets/img/5.jpg";
import car6Image from "../assets/img/6.jpg";
import { Grid } from "@mui/material";
import CTASection from "../assets/components/CTASection";
import { NavbarLogin } from "../assets/components/NavbarLogin";

export const LandingPageLogin = () => {
  return (
    <div>
      {/* Gunakan CssBaseline untuk menghilangkan margin dan padding secara default */}
      <CssBaseline />
      <NavbarLogin />
      <Jumbotron />
      <FeatureSection>
        <SectionTitle />
        <Grid container spacing={2}>
          <Grid item xs={4}>
            <CarCard
              image={car1Image}
              carType="SUV"
              price="IDR 700.000"
              carName="Course SUV Kijang Innova"
            />
          </Grid>
          <Grid item xs={4}>
            <CarCard
              image={car2Image}
              carType="LGGC"
              price="IDR 500.000"
              carName="Course LGGC Honda Brio"
            />
          </Grid>
          <Grid item xs={4}>
            <CarCard
              image={car3Image}
              carType="SUV"
              price="IDR 800.000"
              carName="Hyundai Palisade 2021"
            />
          </Grid>
          <Grid item xs={4}>
            <CarCard
              image={car4Image}
              carType="SUV"
              price="IDR 800.000"
              carName="Course Mitsubishi Pajero"
            />
          </Grid>
          <Grid item xs={4}>
            <CarCard
              image={car5Image}
              carType="Truck"
              price="IDR 1.200.000"
              carName="Dump Truck for Mining Constructor"
            />
          </Grid>
          <Grid item xs={4}>
            <CarCard
              image={car6Image}
              carType="Sedan"
              price="IDR 400.000"
              carName="Sedan Honda Civic"
            />
          </Grid>
        </Grid>
      </FeatureSection>
      <CTASection />
      <Footer />
    </div>
  );
};
