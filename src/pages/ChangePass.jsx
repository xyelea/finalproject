import React, { useState } from "react";
import {
  Alert,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
} from "@mui/material";
import ButtonComponent from "../assets/components/Button";
import TextFieldComponent from "../assets/components/TextField";
import { Navbar } from "../assets/components/Navbar";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import { useNavigate, useParams } from "react-router-dom";
import { useLocation } from "react-router-dom";
import axios from "axios";
import { Visibility, VisibilityOff } from "@mui/icons-material";

export const ChangePass = () => {
  const navigate = useNavigate();
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const apilink = process.env.REACT_APP_BASE_URL;
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const email = searchParams.get("email");
  const id = searchParams.get("id");

  const is600H = window.innerHeight <= 600;
  const is900H = window.innerHeight <= 900;

  const changePass = async () => {
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if (
      password === "" ||
      confirmPassword === "" ||
      (password === "" && confirmPassword === "")
    ) {
      setError("Please fill in all fields.");
    } else if (!passwordRegex.test(password)) {
      setError(
        "Invalid password. Password should contain at least 8 characters, including one letter and one number."
      );
    } else if (password !== confirmPassword) {
      setError("Password and confirm password do not match.");
    } else {
      try {
        const data = {
          password: password,
        };
        const response = await axios.put(
          `${apilink}/User/changepass?email=${email}`,
          data
        );
      } catch (error) {
        console.log(error);
      }
      navigate("/login");
    }
  };
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleClickShowConfirmPassword = () =>
    setShowConfirmPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (
    <div>
      <Navbar />

      <Grid
        container
        justifyContent="center"
        alignItems="center"
        sx={{ paddingTop: is600H ? 10 : is900H ? 20 : 40 }}>
        <Grid item md={6} xs={8}>
          <Stack spacing={2} width="100%">
            <Typography
              variant="h4"
              style={{
                marginBottom: "10px",
                textAlign: "start",
                color: "#790B0A",
              }}>
              Create Password
            </Typography>

            <FormControl variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={showPassword ? "text" : "password"}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end">
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormControl>
            <FormControl variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Confirm Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={showConfirmPassword ? "text" : "password"}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowConfirmPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end">
                      {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="Confirm Password"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
            </FormControl>

            {error && (
              <Alert
                severity="error"
                sx={{ color: "red", marginTop: "5px", textAlign: "center" }}>
                {error}
              </Alert>
            )}

            <Grid
              item
              container
              justifyContent="flex-end"
              style={{
                display: "flex",
                justifyContent: "end",
              }}>
              <Grid item style={{ marginRight: "10pX" }}>
                <ButtonComponent text="Cancel" variant="outlined" />
              </Grid>
              <ButtonComponent
                to=""
                text="Confirm"
                variant="contained"
                style={{}}
                onClick={changePass}
              />
            </Grid>
          </Stack>
        </Grid>
      </Grid>
    </div>
  );
};
