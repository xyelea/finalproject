import React from "react";
import { Box, Button, Container, Grid, Typography } from "@mui/material";
import ImgSuccess from "../assets/img/success.png";
import { BasicNavbar } from "../assets/components/BasicNavbar";
import ButtonComponent from "../assets/components/Button";
import HomeIcon from "@mui/icons-material/Home";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";

export const SuccessPurchase = () => {
  return (
    <Box>
      <BasicNavbar></BasicNavbar>
      <Container
        sx={{
          width: { md: "50%", sm: "80%", xs: "100%" },
          mt: "160px",
        }}
      >
        <Box display={"flex"} flexDirection={"column"} alignItems={"center"}>
          <img alt="success logo" src={ImgSuccess}></img>
          <Typography
            variant="h5"
            sx={{
              fontWeight: "500",
              fontSize: {
                md: "24px",
                sm: "24px",
                xs: "16px",
              },
              color: "#790B0A",
              mt: "44px",
            }}
          >
            Purchase Successfully
          </Typography>

          <Typography
            fontFamily={"Montserrat"}
            variant="subtitle1"
            sx={{
              fontSize: {
                md: "16px",
                sm: "16px",
                xs: "12px",
              },
              color: "#4F4F4F",
              mb: "40px",
              mt: "8px",
            }}
          >
            That’s Great! We’re ready for driving day
          </Typography>

          <Grid container spacing={3} justifyContent={"center"}>
            <Grid item>
              <ButtonComponent
                variant="outlined"
                to="/"
                text="Back to Home"
                startIcon={<HomeIcon></HomeIcon>}
              ></ButtonComponent>
            </Grid>
            <Grid item>
              <ButtonComponent
                variant="contained"
                to="/invoice"
                text="Open Invoice"
                startIcon={<ArrowForwardIcon></ArrowForwardIcon>}
              ></ButtonComponent>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Box>
  );
};
