import React from "react";
import { Box, Button, Container, Typography } from "@mui/material";
import ImgSuccess from "../assets/img/success.png";
import { BasicNavbar } from "../assets/components/BasicNavbar";
import ButtonComponent from "../assets/components/Button";

export const EmailConfirmSuccess = () => {
  return (
    <Box>
      <BasicNavbar></BasicNavbar>
      <Container
        sx={{
          width: { md: "50%", sm: "80%", xs: "100%" },
          mt: "160px",
        }}
      >
        <Box display={"flex"} flexDirection={"column"} alignItems={"center"}>
          <img alt="success logo" src={ImgSuccess}></img>
          <Typography
            variant="h5"
            sx={{
                fontWeight:"500",
              fontSize: {
                md: "24px",
                sm: "24px",
                xs: "16px",
              },
              color: "#790B0A",
              mt: "44px",
            }}
          >
            Email Confirmation Success
          </Typography>

          <Typography
            fontFamily={"Montserrat"}
            variant="subtitle1"
            sx={{
              fontSize: {
                md: "16px",
                sm: "16px",
                xs: "12px",
              },
              textAlign:"center",
              color: "#4F4F4F",
              mb: "40px",
              mt: "8px",
            }}
          >
            Your email already! Please login first to access the web
          </Typography>

          <ButtonComponent
            variant="contained"
            to="/login"
            text="Login Here"
          >
          </ButtonComponent>
        </Box>
      </Container>
    </Box>
  );
};
