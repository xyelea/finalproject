import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import MenuIcon from "@mui/icons-material/Menu";
import MuiAppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import MuiDrawer from "@mui/material/Drawer";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Link from "@mui/material/Link";
import List from "@mui/material/List";
import Paper from "@mui/material/Paper";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { ThemeProvider, createTheme, styled } from "@mui/material/styles";
import axios from "axios";
import * as React from "react";
import { useEffect, useState } from "react";
import ButtonComponent from "../assets/components/Button";
import {
  UserDialogPayAdd,
  UserDialogPayEdit,
} from "../assets/components/UserDialog";
import { Notif } from "../assets/components/Notif";
import { SecondaryListItems } from "../assets/components/SecondaryListItems";
import { MainListItems } from "../assets/components/MainListItems";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="/">
        Otomobil
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  "& .MuiDrawer-paper": {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxSizing: "border-box",
    ...(!open && {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9),
      },
    }),
  },
}));

// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme();

const PaymentMethod = () => {
  const [open, setOpen] = React.useState(true);
  const [openDialogAdd, setOpenDialogAdd] = useState(false);
  const [openDialogEdit, setOpenDialogEdit] = useState(false);
  const [payment, setPayment] = useState([]);
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [image, setImage] = useState("");
  const [status, setStatus] = useState(false);
  const [nameError, setNameError] = useState(false);
  const [imageError, setImageError] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarColor, setSnackbarColor] = useState("#FF0000");
  const [is550W, setIs550W] = useState(window.innerWidth <= 550);
  const apilink = process.env.REACT_APP_BASE_URL;
  const fetchData = async () => {
    try {
      const response = await axios.get(`${apilink}/paymentmethods`);
      setPayment(response.data);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    fetchData();
    const handleResize = () => {
      setIs550W(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const postData = async () => {
    try {
      const data = {
        name: name,
        image: image,
        isActive: status,
      };

      const response = await axios.post(`${apilink}/paymentmethods`, data);
      setSnackbarMessage(response.data);
      setSnackbarColor("green");
      setSnackbarOpen(true);
    } catch (error) {
      console.log(error);
    }
  };
  const postDataUpdated = async () => {
    try {
      const data = {
        idPaymentMethod: id,
        name: name,
        image: image,
        isActive: status,
      };

      const response = await axios.put(`${apilink}/paymentmethods/${id}`, data);
      setSnackbarMessage(response.data);
      setSnackbarColor("green");
      setSnackbarOpen(true);
    } catch (error) {
      console.log(error);
    }
  };

  const toggleDrawer = () => {
    setOpen(!open);
  };

  const handleOpenDialogAdd = () => {
    setOpenDialogAdd(true);
  };
  const handleOpenDialogEdit = (payment) => {
    if (payment) {
      const { idPaymentMethod, name, image, isActive } = payment;
      setId(idPaymentMethod);
      setName(name);
      setImage(image);
      setStatus(isActive);
    }
    setOpenDialogEdit(true);
  };

  const handleCancelDialog = () => {
    setOpenDialogAdd(false);
    setOpenDialogEdit(false);
    setId("");
    setName("");
    setImage("");
    setStatus(false);
    setNameError(false);
    setImageError(false);
  };
  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleImageChange = (e) => {
    setImage(e.target.value);
  };

  const handleStatusChange = (e) => {
    setStatus(e.target.value);
  };
  const handleSave = async () => {
    if (validateInputs()) {
      await postData();
      setOpenDialogAdd(false);
      setId("");
      setName("");
      setImage("");
      setStatus(false);
      fetchData();
    }
  };
  const handleSaveEdit = async () => {
    if (validateInputs()) {
      await postDataUpdated();
      setOpenDialogEdit(false);
      setId("");
      setName("");
      setImage("");
      setStatus(false);
      fetchData();
    }
  };
  const handleNotif = () => {
    setSnackbarOpen(false);
  };
  const validateInputs = () => {
    let isValid = true;

    if (name.trim() === "") {
      setNameError(true);
      isValid = false;
    } else {
      setNameError(false);
    }

    if (image.trim() === "") {
      setImageError(true);
      isValid = false;
    } else {
      setImageError(false);
    }

    return isValid;
  };

  return (
    <ThemeProvider theme={defaultTheme}>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar position="absolute" open={open} sx={{ bgcolor: "#790B0A" }}>
          <Toolbar
            sx={{
              pr: "24px", // keep right padding when drawer closed
            }}
          >
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              sx={{
                marginRight: "36px",
                ...(open && { display: "none" }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              Dashboard
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
          <Toolbar
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end",
              px: [1],
            }}
          >
            <IconButton onClick={toggleDrawer}>
              <ChevronLeftIcon />
            </IconButton>
          </Toolbar>
          <Divider />
          <List component="nav">
            <MainListItems />
            <Divider sx={{ my: 1 }} />
            <SecondaryListItems />
          </List>
        </Drawer>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === "light"
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
            marginLeft: is550W ? (open ? drawerWidth : 0) : "",
            transition: defaultTheme.transitions.create("margin", {
              easing: defaultTheme.transitions.easing.sharp,
              duration: defaultTheme.transitions.duration.leavingScreen,
            }),
          }}
        >
          <Toolbar />
          <Container sx={{ mt: 4, mb: 4 }}>
            <Grid container>
              <Grid item xs={12} md={12} lg={12}>
                <Paper
                  sx={{
                    p: 2,
                    display: "flex",
                    flexDirection: "column",
                    height: "100%",
                    width: "100%",
                  }}
                >
                  <div>
                    <Notif
                      snackbarOpen={snackbarOpen}
                      handleNotif={handleNotif}
                      snackbarColor={snackbarColor}
                      snackbarMessage={snackbarMessage}
                    />
                    <UserDialogPayAdd
                      openPaymentAdd={openDialogAdd}
                      handleCancel={handleCancelDialog}
                      name={name}
                      image={image}
                      status={status}
                      handleNameChange={handleNameChange}
                      handleImageChange={handleImageChange}
                      handleStatusChange={handleStatusChange}
                      nameError={nameError}
                      imageError={imageError}
                      handleSave={handleSave}
                    />
                    <UserDialogPayEdit
                      openPaymentEdit={openDialogEdit}
                      handleCancel={handleCancelDialog}
                      name={name}
                      image={image}
                      status={status}
                      handleNameChange={handleNameChange}
                      handleImageChange={handleImageChange}
                      handleStatusChange={handleStatusChange}
                      nameError={nameError}
                      imageError={imageError}
                      handleSaveEdit={handleSaveEdit}
                    />
                    <Typography variant="h4" mb="20px">
                      Payment Methods
                    </Typography>
                    <ButtonComponent
                      text="Add Payment"
                      variant="contained"
                      to={null}
                      onClick={handleOpenDialogAdd}
                      cStyle={{
                        bgcolor: "orange",
                        "&:hover": {
                          backgroundColor: "orange",
                        },
                        marginBottom: "20px",
                      }}
                    />

                    {payment.map((payment) => (
                      <React.Fragment key={payment.idPaymentMethod}>
                        <Divider />
                        <Grid
                          item
                          display="flex"
                          justifyContent="space-between"
                          paddingLeft="20px"
                          paddingRight="20px"
                          height={"60px"}
                          alignItems={"center"}
                          width={"100%"}
                        >
                          <Grid item xs={2} md={5} display="flex">
                            <img
                              src={payment.image}
                              style={{
                                width: "50px",
                                height: "50px",
                                borderRadius: "50px",
                              }}
                            ></img>
                            <Grid
                              item
                              ml={"10px"}
                              xs={5}
                              md={5}
                              display={"flex"}
                              flexDirection={"column"}
                            >
                              <Grid
                                item
                                xs={12}
                                md={12}
                                display={"flex"}
                                justifyContent={"start"}
                              >
                                <Typography align="center">
                                  {payment.name}
                                </Typography>
                              </Grid>
                              <Grid item xs={4} md={2}>
                                <Typography align="center">
                                  {payment.isActive ? "Enabled" : "Disabled"}
                                </Typography>
                              </Grid>
                            </Grid>
                          </Grid>
                          <Grid
                            item
                            xs={4}
                            md={2}
                            display="flex"
                            justifyContent={"center"}
                          ></Grid>
                          <Grid
                            container
                            item
                            xs={4}
                            md={2}
                            justifyContent={"center"}
                          >
                            <ButtonComponent
                              text="Edit"
                              variant="contained"
                              to={null}
                              onClick={() => handleOpenDialogEdit(payment)}
                              cStyle={{
                                bgcolor: "orange",
                                "&:hover": {
                                  backgroundColor: "orange",
                                },
                              }}
                            />
                          </Grid>
                        </Grid>
                      </React.Fragment>
                    ))}
                    <Grid
                      container
                      item
                      xs={12}
                      md={12}
                      lg={12}
                      justifyContent="space-between"
                      alignItems="center"
                      sx={{ mt: 2 }}
                    ></Grid>
                  </div>
                </Paper>
              </Grid>
            </Grid>
            <Copyright sx={{ pt: 4 }} />
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  );
};
export default PaymentMethod;
