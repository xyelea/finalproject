import {
  Alert,
  FormControl,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
} from "@mui/material";
import Typography from "@mui/material/Typography";
import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import ButtonComponent from "../assets/components/Button";
import { Navbar } from "../assets/components/Navbar";
import TextFieldComponent from "../assets/components/TextField";
import Stack from "@mui/material/Stack";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { AuthContext } from "../context/AuthProvider";
import { Visibility, VisibilityOff } from "@mui/icons-material";

export const Login = () => {
  const apilink = process.env.REACT_APP_BASE_URL;
  const { token, user } = useContext(AuthContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const { login } = useContext(AuthContext);
  const navigate = useNavigate();
  const [is600H, setIs600H] = useState(window.innerHeight <= 600);
  const [is900H, setIs900H] = useState(window.innerHeight <= 900);

  if (token && user.role === "member") {
    navigate("/");
  } else if (token && user.role === "admin") {
    navigate("/adminpanel");
  }

  const handleLogin = () => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

    if (email === "" && password === "") {
      setError("Please enter both email and password.");
    } else if (email === "") {
      setError("Please enter the email.");
    } else if (password === "") {
      setError("Please enter the password.");
    } else if (!emailRegex.test(email)) {
      setError("Invalid email. Please enter a valid email address.");
    } else if (!passwordRegex.test(password)) {
      setError(
        "Invalid password. Password should contain at least 8 characters, including one letter and one number."
      );
    } else {
      axios
        .post(`${apilink}/user/login`, {
          email: email,
          password: password,
        })
        .then((response) => {
          // Simpan token dalam local storage
          localStorage.setItem("token", response.data.token);
          const testoken = response.data.token;
          const decoded = jwt_decode(testoken);

          // Lakukan sesuatu dengan respons yang diterima

          login(response.data.token);
          // Memanggil fungsi login dari AuthContext dengan memberikan token
          if (decoded.role === "admin") {
            navigate("/adminpanel");
          } else {
            navigate("/landingpage");
          }
          setEmail("");
          setPassword("");
          setError("");
        })
        .catch((error) => {
          // Tangani respons error dari server
          // Lakukan sesuatu dengan respons error yang diterima
          setError("Invalid email or password. Please check your credentials.");
        });
    }
  };
  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  useEffect(() => {
    const handleResize = () => {
      setIs600H(window.innerHeight <= 600);
      setIs900H(window.innerHeight <= 900);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div>
      <Navbar />
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        sx={{ paddingTop: is600H ? 10 : is900H ? 15 : 30 }}
      >
        <Grid item md={6} xs={8}>
          <Stack spacing={2} width="100%">
            <Typography
              variant="h4"
              style={{
                marginBottom: "10px",
                textAlign: "start",
                color: "#790B0A",
              }}
            >
              Welcome Back!
            </Typography>
            <Typography
              variant="body2"
              sx={{
                marginBottom: is600H ? "" : "20px",
                textAlign: "start",
              }}
            >
              Please login first
            </Typography>

            <TextFieldComponent
              label="Email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />

            <FormControl variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={showPassword ? "text" : "password"}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
                label="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormControl>
          </Stack>
          {error && (
            <Alert
              severity="error"
              sx={{ color: "red", marginTop: "5px", textAlign: "center" }}
            >
              {error}
            </Alert>
          )}
          <Grid
            item
            container
            direction="column"
            alignItems="center"
            marginTop="20px"
            spacing={2}
          >
            <Grid item container justifyContent="start">
              <Typography variant="body2">
                Forgot password?{" "}
                <Link
                  to="/resetpass"
                  style={{
                    textDecoration: "none",
                    color: "#2F80ED",
                    cursor: "pointer",
                  }}
                >
                  Click here
                </Link>
              </Typography>
            </Grid>
            <Grid item container justifyContent="end">
              <ButtonComponent
                text="Login"
                variant="contained"
                onClick={handleLogin}
              />
            </Grid>
            <Grid item>
              <Typography variant="body2">
                Don't have an account?{" "}
                <Link
                  to="/signup"
                  style={{
                    textDecoration: "none",
                    color: "#2F80ED",
                    cursor: "pointer",
                  }}
                >
                  Sign Up Here
                </Link>
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};
