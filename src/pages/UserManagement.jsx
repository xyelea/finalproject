import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import MenuIcon from "@mui/icons-material/Menu";
import MuiAppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import MuiDrawer from "@mui/material/Drawer";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Link from "@mui/material/Link";
import List from "@mui/material/List";
import Paper from "@mui/material/Paper";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { ThemeProvider, createTheme, styled } from "@mui/material/styles";
import axios from "axios";
import * as React from "react";
import { useEffect, useState } from "react";
import ButtonComponent from "../assets/components/Button";
import { UserDialogAdd, UserDialogEdit } from "../assets/components/UserDialog";
import { Notif } from "../assets/components/Notif";
import { InvoiceCardUserManagement } from "../assets/components/InvoiceCard";
import { SecondaryListItems } from "../assets/components/SecondaryListItems";
import { MainListItems } from "../assets/components/MainListItems";
function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.Secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="/">
        Otomobil
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  "& .MuiDrawer-paper": {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxSizing: "border-box",
    ...(!open && {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9),
      },
    }),
  },
}));

// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme();

const UserManagement = () => {
  const [open, setOpen] = React.useState(true);
  const [openDialogAdd, setOpenDialogAdd] = useState(false);
  const [openDialogEdit, setOpenDialogEdit] = useState(false);
  const [users, setUsers] = useState([]);
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");
  const [status, setStatus] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);

  const [perPage] = useState(5);
  const [nameError, setNameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  const [nameErrorMessage, setNameErrorMessage] = useState("");
  const [passwordError, setPasswordError] = useState(false);
  const [snackbarOpen, setSnackbarOpen] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState("");
  const [snackbarColor, setSnackbarColor] = useState("#FF0000");

  const apilink = process.env.REACT_APP_BASE_URL;
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);
  const [is550W, setIs550W] = useState(window.innerWidth <= 550);

  useEffect(() => {
    const handleResize = () => {
      setIs600W(window.innerWidth <= 600);
      setIs550W(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  const fetchData = async () => {
    try {
      const response = await axios.get(
        `${apilink}/UserAdmin?limit=${currentPage * 5}`
      );
      setUsers(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, [currentPage]);

  const postData = async () => {
    try {
      const data = {
        username: name,
        password: password,
        email: email,
      };
      const response = await axios.post(`${apilink}/UserAdmin`, data);
      setSnackbarMessage(response.data);
      setSnackbarColor("green");
      setSnackbarOpen(true);
    } catch (error) {
      if (error.response && error.response.data) {
        setSnackbarMessage(error.response.data);
        setSnackbarColor("red");
      } else {
        setSnackbarMessage("An error occurred while adding the user.");
        setSnackbarColor("red");
      }
      setSnackbarOpen(true);
    }
  };
  const postDataUpdated = async () => {
    try {
      const data = {
        id: id,
        username: name,
        email: email,
        password: password,
        role: role,
        isActive: status,
      };

      const response = await axios.put(`${apilink}/UserAdmin/${id}`, data);
      setSnackbarMessage(response.data);
      setSnackbarColor("green");
      setSnackbarOpen(true);
    } catch (error) {
      if (
        error.response &&
        error.response.data &&
        error.response.data.errorMessage
      ) {
        setSnackbarMessage(error.response.data.errorMessage);
        setSnackbarColor("red");
      } else {
        setSnackbarMessage("An error occurred while updating the user.");
        setSnackbarColor("red");
      }
      setSnackbarOpen(true);
    }
  };

  const toggleDrawer = () => {
    setOpen(!open);
  };
  const handleNextPage = () => {
    setCurrentPage((prevPage) => prevPage + 1);
  };

  const handlePrevPage = () => {
    setCurrentPage((prevPage) => prevPage - 1);
  };
  const handleOpenDialogAdd = () => {
    setOpenDialogAdd(true);
  };
  const handleOpenDialogEdit = (user) => {
    setId(user.id);
    setName(user.username);
    setEmail(user.email);
    setPassword(user.password);
    setRole(user.role);
    setStatus(user.isActive);
    setOpenDialogEdit(true);
  };
  const handleCancelDialog = () => {
    setOpenDialogAdd(false);
    setOpenDialogEdit(false);
    setName("");
    setEmail("");
    setPassword("");
    setRole(false);
    setNameError(false);
    setEmailError(false);
    setPasswordError(false);
  };
  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };
  const handleRoleChange = (e) => {
    setRole(e.target.value);
  };
  const handleStatusChange = (e) => {
    setStatus(e.target.value);
  };
  const handleSave = async () => {
    if (validateInputs()) {
      await postData();
      setName("");
      setEmail("");
      setPassword("");
      setOpenDialogAdd(false);
      fetchData();
    }
  };

  const handleSaveEdit = async () => {
    if (validateInputs()) {
      await postDataUpdated();
      setOpenDialogEdit(false);
      setName("");
      setEmail("");
      setPassword("");
      setRole(false);
      setStatus(false);
      fetchData();
    }
  };
  const handleNotif = () => {
    setSnackbarOpen(false);
  };
  const validateInputs = () => {
    let isValid = true;

    if (name.trim() === "") {
      setNameErrorMessage("Name cannot be empty");
      setNameError(true);
      isValid = false;
    } else if (!/^.{4,}$/.test(name)) {
      setNameErrorMessage("Name should be more than 4 Character");
      setNameError(true);
      isValid = false;
    } else {
      setNameError(false);
    }

    if (email.trim() === "") {
      setEmailError(true);
      setEmailErrorMessage("Email cannot be empty");
      isValid = false;
    } else if (!/^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email.trim())) {
      setEmailError(true);
      setEmailErrorMessage("Email is not valid");
      isValid = false;
    } else {
      setEmailError(false);
    }

    if (password.trim() === "") {
      setPasswordError(true);
      isValid = false;
    } else {
      setPasswordError(false);
    }

    return isValid;
  };
  const indexOfLastUser = currentPage * perPage;
  const indexOfFirstUser = indexOfLastUser - perPage;
  const currentUsers = users.slice(indexOfFirstUser, indexOfLastUser);

  return (
    <ThemeProvider theme={defaultTheme}>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar position="absolute" open={open} sx={{ bgcolor: "#790B0A" }}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              sx={{
                marginRight: "36px",
                ...(open && { display: "none" }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              Dashboard
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
          <Toolbar
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end",
              px: [1],
            }}
          >
            <IconButton onClick={toggleDrawer}>
              <ChevronLeftIcon />
            </IconButton>
          </Toolbar>
          <Divider />
          <List component="nav">
            <MainListItems />
            <Divider sx={{ my: 1 }} />
            <SecondaryListItems />
          </List>
        </Drawer>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === "light"
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
            marginLeft: is550W ? (open ? drawerWidth : 0) : "",
            transition: defaultTheme.transitions.create("margin", {
              easing: defaultTheme.transitions.easing.sharp,
              duration: defaultTheme.transitions.duration.leavingScreen,
            }),
          }}
        >
          <Toolbar />
          <Container sx={{ mt: 4, mb: 4 }}>
            <Grid container>
              <Grid item xs={12} md={12} lg={12}>
                <Paper
                  sx={{
                    p: 2,
                    display: "flex",
                    flexDirection: "column",
                    height: "100%",
                    width: "100%",
                  }}
                >
                  <div>
                    <Notif
                      snackbarOpen={snackbarOpen}
                      handleNotif={handleNotif}
                      snackbarColor={snackbarColor}
                      snackbarMessage={snackbarMessage}
                    />
                    <UserDialogAdd
                      openDialogAdd={openDialogAdd}
                      handleCancel={handleCancelDialog}
                      name={name}
                      email={email}
                      password={password}
                      handleNameChange={handleNameChange}
                      handleEmailChange={handleEmailChange}
                      handlePasswordChange={handlePasswordChange}
                      handleSave={handleSave}
                      nameError={nameError}
                      emailError={emailError}
                      passwordError={passwordError}
                      emailErrorMessage={emailErrorMessage}
                      nameErrorMessage={nameErrorMessage}
                    />
                    <UserDialogEdit
                      openDialogEdit={openDialogEdit}
                      handleCancel={handleCancelDialog}
                      name={name}
                      email={email}
                      password={password}
                      handleNameChange={handleNameChange}
                      handleEmailChange={handleEmailChange}
                      handlePasswordChange={handlePasswordChange}
                      nameError={nameError}
                      emailError={emailError}
                      passwordError={passwordError}
                      role={role}
                      status={status}
                      handleRoleChange={handleRoleChange}
                      handleStatusChange={handleStatusChange}
                      handleSaveEdit={handleSaveEdit}
                      emailErrorMessage={emailErrorMessage}
                      nameErrorMessage={nameErrorMessage}
                    />
                    <Typography variant="h4" mb="20px">
                      User Management
                    </Typography>
                    <ButtonComponent
                      text="Add User"
                      variant="contained"
                      to={null}
                      onClick={handleOpenDialogAdd}
                      cStyle={{
                        bgcolor: "orange",
                        "&:hover": {
                          backgroundColor: "orange",
                        },
                      }}
                    />
                    {is600W ? (
                      ""
                    ) : (
                      <Grid
                        item
                        display="flex"
                        justifyContent="space-between"
                        paddingLeft="20px"
                        paddingRight="20px"
                        bgcolor="#790B0A"
                        height={"60px"}
                        alignItems={"center"}
                        width={"100%"}
                        sx={{ marginTop: "24px" }}
                      >
                        <Grid item xs={2} md={5}>
                          <Typography color="#fff" align="center">
                            Email
                          </Typography>
                        </Grid>
                        <Grid item xs={4} md={2}>
                          <Typography color="#fff" align="center">
                            Name
                          </Typography>
                        </Grid>
                        <Grid item xs={4} md={2}>
                          <Typography color="#fff" align="center">
                            Rule
                          </Typography>
                        </Grid>
                        <Grid item xs={4} md={2}>
                          <Typography color="#fff" align="center">
                            Status
                          </Typography>
                        </Grid>
                        <Grid item xs={4} md={2}>
                          <Typography color="#fff" align="center">
                            Action
                          </Typography>
                        </Grid>
                      </Grid>
                    )}
                    {currentUsers &&
                      currentUsers.map((user) => (
                        <InvoiceCardUserManagement
                          handleOpenDialogEdit={() =>
                            handleOpenDialogEdit(user)
                          }
                          id={user.id}
                          email={user.email}
                          username={user.username}
                          role={user.role}
                          isActive={user.isActive}
                        />
                      ))}
                    <Grid
                      container
                      item
                      xs={12}
                      md={12}
                      lg={12}
                      justifyContent="space-between"
                      alignItems="center"
                      sx={{ mt: 2 }}
                    >
                      {!(currentUsers.length < 5 && currentPage === 1) && (
                        <>
                          <ButtonComponent
                            onClick={handlePrevPage}
                            disabled={currentPage === 1}
                            variant="contained"
                            text="Previous"
                            cStyle={{
                              bgcolor: "orange",
                              "&:hover": {
                                backgroundColor: "orange",
                              },
                            }}
                          >
                            Previous
                          </ButtonComponent>
                          <Typography>{currentPage}</Typography>
                          <ButtonComponent
                            onClick={handleNextPage}
                            disabled={currentUsers.length < perPage}
                            variant="contained"
                            text="Next"
                            cStyle={{
                              bgcolor: "orange",
                              "&:hover": {
                                backgroundColor: "orange",
                              },
                            }}
                          >
                            Next
                          </ButtonComponent>
                        </>
                      )}
                    </Grid>
                  </div>
                </Paper>
              </Grid>
            </Grid>
            <Copyright sx={{ pt: 4 }} />
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  );
};
export default UserManagement;
