import React, { useEffect, useState, useContext } from "react";
import { Navbar } from "../assets/components/Navbar";
import Jumbotron from "../assets/components/Jumbotron";
import CssBaseline from "@mui/material/CssBaseline";
import FeatureSection from "../assets/components/FeatureSection";
import Footer from "../assets/components/Footer";
import CarCard from "../assets/components/CarCard";
import SectionTitle from "../assets/components/SectionTitle";
import { Box, Grid } from "@mui/material";
import CTASection from "../assets/components/CTASection";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { NavbarLogin } from "../assets/components/NavbarLogin";
import { AuthContext } from "../context/AuthProvider";
import { NavbarAdmin } from "../assets/components/NavbarAdmin";

export const Landingpage = () => {
  const apilink = process.env.REACT_APP_BASE_URL;
  const { token, user } = useContext(AuthContext);
  const navigate = useNavigate();
  const [course, setCourse] = useState([]);
  const getCourse = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/Course`,

      headers: {
        "Content-Type": "application/json",
      },
    };

    axios
      .request(config)
      .then((response) => {
        setCourse(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const renderNavbar = () => {
    if (user && user.role === "member") {
      return <NavbarLogin />;
    } else if (user && user.role === "admin") {
      return <NavbarAdmin />;
    } else {
      return <Navbar />;
    }
  };

  const renderCourse = () => {
    return course.map((val) => {
      return (
        <Grid key={val.id} item xs={12} sm={6} md={4}>
          <Box onClick={() => navigate(`/course-detail/${val.id}`)}>
            <CarCard
              image={val.image}
              carType={val.category}
              price={val.price}
              carName={val.name}
            />
          </Box>
        </Grid>
      );
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    getCourse();
  }, []);

  return (
    <div style={{ position: "flex" }}>
      {/* Use CssBaseline to remove default margins and paddings */}
      <CssBaseline />
      {renderNavbar()}
      <Jumbotron />
      <FeatureSection>
        <SectionTitle text="Join us for the course" />
        <Grid container flexWrap={"wrap"} spacing={2}>
          <Grid item xs={12}>
            <SectionTitle title="Join us for the Course" />
          </Grid>
          {renderCourse()}
        </Grid>
      </FeatureSection>
      <CTASection />
      <Footer />
    </div>
  );
};
