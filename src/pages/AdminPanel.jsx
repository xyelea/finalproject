import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import MenuIcon from "@mui/icons-material/Menu";
import MuiAppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import MuiDrawer from "@mui/material/Drawer";
import Grid from "@mui/material/Grid";
import IconButton from "@mui/material/IconButton";
import Link from "@mui/material/Link";
import List from "@mui/material/List";
import Paper from "@mui/material/Paper";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { ThemeProvider, createTheme, styled } from "@mui/material/styles";
import axios from "axios";
import * as React from "react";
import { useEffect, useState } from "react";
import {
  AdminInvoicePanel,
  CardPreviewAdmin,
} from "../assets/components/InvoiceCard";
import { Navigate, Redirect, useNavigate } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "../context/AuthProvider";
import { SecondaryListItems } from "../assets/components/SecondaryListItems";
import { MainListItems } from "../assets/components/MainListItems";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="/">
        Otomobil
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  "& .MuiDrawer-paper": {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    boxSizing: "border-box",
    ...(!open && {
      overflowX: "hidden",
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(7),
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9),
      },
    }),
  },
}));

// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme();

const AdminPanel = () => {
  const navigate = useNavigate();
  const { token, user } = useContext(AuthContext);
  const [open, setOpen] = React.useState(true);
  const [users, setUsers] = useState([]);
  const [payment, setPayment] = useState([]);
  const [invoice, setInvoice] = useState([]);
  const apilink = process.env.REACT_APP_BASE_URL;
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);
  const [is550W, setIs550W] = useState(window.innerWidth <= 550);

  useEffect(() => {
    const handleResize = () => {
      setIs600W(window.innerWidth <= 600);
      setIs550W(window.innerWidth <= 550);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const fetchUser = async () => {
    try {
      const response = await axios.get(`${apilink}/UserAdmin?limit=5`);
      setUsers(response.data);
    } catch (error) {
      console.log(error);
    }
  };
  const fetchPayment = async () => {
    try {
      const response = await axios.get(`${apilink}/paymentmethods`);
      setPayment(response.data);
    } catch (error) {
      console.log(error);
    }
  };
  const fetchInvoice = async () => {
    try {
      const response = await axios.get(`${apilink}/InvoiceAdmin?limit=5`);
      setInvoice(response.data);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    fetchUser();
    fetchPayment();
    fetchInvoice();
  }, []);

  const toggleDrawer = () => {
    setOpen(!open);
  };

  if (!token && !user) {
    return <Navigate to="/" />;
  }

  return (
    <ThemeProvider theme={defaultTheme}>
      <CssBaseline />
      <Box sx={{ display: "flex" }}>
        <AppBar position="absolute" open={open} sx={{ bgcolor: "#790B0A" }}>
          <Toolbar
            sx={{
              pr: "24px", // keep right padding when drawer closed
            }}
          >
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              sx={{
                marginRight: "36px",
                ...(open && { display: "none" }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              Dashboard
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
          <Toolbar
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end",
              px: [1],
            }}
          >
            <IconButton onClick={toggleDrawer}>
              <ChevronLeftIcon />
            </IconButton>
          </Toolbar>
          <Divider />
          <List component="nav">
            <MainListItems />
            <Divider sx={{ my: 1 }} />
            <SecondaryListItems />
          </List>
        </Drawer>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === "light"
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
            marginLeft: is550W ? (open ? drawerWidth : 0) : "",
            transition: defaultTheme.transitions.create("margin", {
              easing: defaultTheme.transitions.easing.sharp,
              duration: defaultTheme.transitions.duration.leavingScreen,
            }),
          }}
        >
          <Toolbar />
          <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={3}>
              {/* Chart */}
              <Grid item xs={12} md={12} lg={8}>
                <Paper
                  sx={{
                    p: 2,
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div>
                    <h1 style={{ display: "flex", justifyContent: "center" }}>
                      User List
                    </h1>
                    {is600W ? null : (
                      <Grid
                        item
                        display="flex"
                        justifyContent="space-between"
                        paddingLeft="20px"
                        paddingRight="20px"
                        bgcolor="#790B0A"
                        height={"60px"}
                        alignItems={"center"}
                        width={"100%"}
                        sx={{ marginTop: "24px" }}
                      >
                        <Grid item xs={4} md={5}>
                          <Typography color="#fff" align="center">
                            Email
                          </Typography>
                        </Grid>
                        <Grid item xs={4} md={2}>
                          <Typography color="#fff" align="center">
                            Name
                          </Typography>
                        </Grid>
                        <Grid item xs={4} md={2}>
                          <Typography color="#fff" align="center">
                            Rule
                          </Typography>
                        </Grid>
                        <Grid item xs={4} md={2}>
                          <Typography color="#fff" align="center">
                            Status
                          </Typography>
                        </Grid>
                      </Grid>
                    )}
                  </div>
                  <div>
                    {users &&
                      users.map((user) => (
                        <CardPreviewAdmin
                          key={user.id}
                          id={user.id}
                          email={user.email}
                          username={user.username}
                          role={user.role}
                          isActive={user.isActive}
                        />
                      ))}
                  </div>
                </Paper>
              </Grid>
              {/* Recent Deposits */}
              <Grid item xs={12} md={12} lg={4}>
                <Paper
                  sx={{
                    p: 2,
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Typography variant="h4" mb="20px">
                    Payment Methods
                  </Typography>
                  {payment.map((payment) => (
                    <React.Fragment key={payment.idPaymentMethod}>
                      <Divider />
                      <Grid
                        item
                        display="flex"
                        justifyContent="space-between"
                        paddingLeft="20px"
                        paddingRight="20px"
                        height={"60px"}
                        alignItems={"center"}
                        width={"100%"}
                      >
                        <Grid item xs={2} md={5} display="flex">
                          <img
                            src={payment.image}
                            style={{
                              width: "50px",
                              height: "50px",
                              borderRadius: "50px",
                            }}
                          ></img>
                          <Grid
                            item
                            ml={"10px"}
                            xs={5}
                            md={5}
                            display={"flex"}
                            flexDirection={"column"}
                          >
                            <Grid
                              item
                              xs={12}
                              md={12}
                              display={"flex"}
                              justifyContent={"start"}
                            >
                              <Typography align="center">
                                {payment.name}
                              </Typography>
                            </Grid>
                            <Grid item xs={4} md={2}>
                              <Typography align="center">
                                {payment.isActive ? "Enabled" : "Disabled"}
                              </Typography>
                            </Grid>
                          </Grid>
                        </Grid>
                        <Grid
                          item
                          xs={4}
                          md={2}
                          display="flex"
                          justifyContent={"center"}
                        ></Grid>
                        <Grid
                          container
                          item
                          xs={4}
                          md={2}
                          justifyContent={"center"}
                        ></Grid>
                      </Grid>
                    </React.Fragment>
                  ))}
                </Paper>
              </Grid>
              {/* Recent Orders */}
              <Grid item xs={12}>
                <Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
                  <Typography variant="h4" mb="20px">
                    Invoice Users
                  </Typography>
                  <Grid
                    item
                    display="flex"
                    justifyContent="space-between"
                    paddingLeft="20px"
                    paddingRight="20px"
                    bgcolor="#790B0A"
                    height="60px"
                    alignItems={"center"}
                    width="100%"
                    sx={{ marginTop: "24px" }}
                  >
                    <Grid item xs={2} md={1}>
                      <Typography color="#fff">No</Typography>
                    </Grid>
                    <Grid item xs={4} md={2}>
                      <Typography color="#fff" align="center">
                        No. Invoice
                      </Typography>
                    </Grid>
                    <Grid item xs={4} md={2}>
                      <Typography color="#fff" align="center">
                        Date
                      </Typography>
                    </Grid>
                    <Grid item xs={4} md={2}>
                      <Typography color="#fff" align="center">
                        Total Course
                      </Typography>
                    </Grid>
                    <Grid item xs={4} md={2}>
                      <Typography color="#fff" align="center">
                        Total Price
                      </Typography>
                    </Grid>
                  </Grid>
                  {invoice.map((invoice) => (
                    <AdminInvoicePanel
                      key={invoice.idInvoice}
                      no={invoice.no}
                      id={invoice.idInvoice}
                      date={invoice.invoiceTanggal}
                      tcourse={invoice.totalCourse}
                      price={invoice.totalCoursePrice}
                    />
                  ))}
                  <Grid
                    container
                    item
                    xs={12}
                    md={12}
                    lg={12}
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{ mt: 2 }}
                  ></Grid>
                </Paper>
              </Grid>
            </Grid>
            <Copyright sx={{ pt: 4 }} />
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  );
};
export default AdminPanel;
