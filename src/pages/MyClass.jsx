import React, { useContext, useEffect, useState } from "react";
import Hyundai from "../assets/img/HyundaiPalisade2021.png";
import { Box, Container, Grid, Typography } from "@mui/material";
import { NavbarLogin } from "../assets/components/NavbarLogin";
import Footer from "../assets/components/Footer";
import axios from "axios";
import moment from "moment";
import { AuthContext } from "../context/AuthProvider";
import { NavbarAdmin } from "../assets/components/NavbarAdmin";
import { Navbar } from "../assets/components/Navbar";

export const MyClass = () => {
  const { token, user } = useContext(AuthContext);
  const apilink = process.env.REACT_APP_BASE_URL;
  const [items, setItems] = useState([]);
  const userId = user.sub;
  const getItems = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/MyClass?UserId=${userId}`,
      headers: {},
    };

    axios
      .request(config)
      .then((response) => {
        setItems(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    getItems();
  }, []);
  const renderNavbar = () => {
    if (user && user.role === "member") {
      return <NavbarLogin />;
    } else if (user && user.role === "admin") {
      return <NavbarAdmin />;
    } else {
      return <Navbar />;
    }
  };
  return (
    <Box>
      {renderNavbar()}
      <Container sx={{ mt: "80px", mb: "280px" }}>
        {items.length !== 0 ? (
          items.map((item) => (
            <Box key={item.id}>
              <Grid
                container
                alignItems={"center"}
                spacing={3}
                sx={{
                  borderBottom: "1px solid #BDBDBD",
                  paddingTop: "24px",
                  paddingBottom: "24px",
                }}
              >
                <Grid item md={2} sm={3} xs={12}>
                  <img
                    alt={item.courseName}
                    src={item.courseImage}
                    width="100%"
                  ></img>
                </Grid>
                <Grid item md={10} sm={9} xs={12}>
                  <Box>
                    <Typography
                      sx={{
                        fontFamily: "montserrat",
                        color: "#828282",
                        fontSize: "16px",
                        fontWeight: "400",
                      }}
                    >
                      {item.courseCategory}
                    </Typography>
                    <Typography
                      sx={{
                        fontFamily: "montserrat",
                        color: "#333333",
                        fontSize: "24px",
                        fontWeight: "600",
                      }}
                    >
                      {item.courseName}
                    </Typography>
                    <Typography
                      sx={{
                        fontFamily: "montserrat",
                        color: "#790B0A",
                        fontSize: "20px",
                        fontWeight: "500",
                      }}
                    >
                      Schedule : {moment(item.date).format("dddd, D MMMM YYYY")}
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          ))
        ) : (
          <Box
            sx={{
              height: "400px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Typography
              sx={{
                fontFamily: "montserrat",
                color: "#333333",
                fontSize: "20px",
                fontWeight: "400",
              }}
            >
              No Course Selected
            </Typography>
          </Box>
        )}
      </Container>
      <Footer />
    </Box>
  );
};
