import React, { useState } from "react";
import { Grid, TextField, Alert } from "@mui/material";
import ButtonComponent from "../assets/components/Button";
import { Navbar } from "../assets/components/Navbar";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import axios from "axios";

export const ResetPass = () => {
  const [error, setError] = useState(null);
  const apilink = process.env.REACT_APP_BASE_URL;
  const is600H = window.innerHeight <= 600;
  const is900H = window.innerHeight <= 900;
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState("");
  const [isEmailSent, setIsEmailSent] = useState(false);

  const validateEmail = () => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!email) {
      setEmailError("Email is required");
    } else if (!emailRegex.test(email)) {
      setEmailError("Invalid email format");
    } else {
      setIsEmailSent(true);
      setEmail("");
      setEmailError("");
      setError(null); // Menghapus pesan error sebelumnya
      resetPassword();
    }
  };

  const resetPassword = () => {
    const data = {
      email: email,
    };

    axios
      .post(`${apilink}/user/resetpass`, data)
      .then((response) => {
        // Berhasil melakukan request
        console.log(response.data);
        // Tambahkan logika atau tindakan sesuai
      })
      .catch((error) => {
        // Error saat melakukan request
        console.error(error);
        // Tambahkan penanganan error
        if (error.response && error.response.data) {
          setError(error.response.data); // Set pesan error dari respons ke state error
        } else {
          setError("An error occurred"); // Pesan error default jika tidak ada respons atau data respons
        }
      });
  };

  return (
    <div>
      <Navbar />
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        sx={{ paddingTop: is600H ? 10 : is900H ? 20 : 40 }}>
        <Grid item md={6} xs={8}>
          <Stack spacing={2} width="100%">
            <Typography
              variant="h4"
              style={{
                marginBottom: "10px",
                textAlign: "start",
                color: "#790B0A",
              }}>
              Reset Password
            </Typography>
            <Typography
              variant="body2"
              style={{ marginBottom: "10px", textAlign: "start" }}>
              Send OTP code to your email address
            </Typography>
            <TextField
              label="Email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              error={Boolean(emailError)}
              helperText={
                emailError && (
                  <Alert severity="error" sx={{ marginTop: "10px" }}>
                    {emailError}
                  </Alert>
                )
              }
            />
            {isEmailSent && !error && (
              <Alert severity="success" sx={{ marginTop: "10px" }}>
                Email sent! Please check your email.
              </Alert>
            )}

            {error && (
              <Alert severity="error" sx={{ marginTop: "10px" }}>
                {error}
              </Alert>
            )}

            <Grid
              item
              container
              justifyContent="flex-end"
              style={{
                display: "flex",
                justifyContent: "end",
              }}>
              <Grid item style={{ marginRight: "10pX" }}>
                <ButtonComponent text="Cancel" variant="outlined" />
              </Grid>

              <ButtonComponent
                to="/changepass"
                text="Confirm"
                variant="contained"
                onClick={validateEmail}
              />
            </Grid>
          </Stack>
        </Grid>
      </Grid>
    </div>
  );
};
