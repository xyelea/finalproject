import React, { useContext } from "react";
import { Grid, Typography } from "@mui/material";
import { NavbarLogin } from "../assets/components/NavbarLogin";
import Footer from "../assets/components/Footer";
import CssBaseline from "@mui/material/CssBaseline";
import InvoiceCard from "../assets/components/InvoiceCard";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import { AuthContext } from "../context/AuthProvider";
import { NavbarAdmin } from "../assets/components/NavbarAdmin";
import { Navbar } from "../assets/components/Navbar";

export const Invoice = () => {
  const { token, user } = useContext(AuthContext);
  const apilink = process.env.REACT_APP_BASE_URL;
  const [is600W, setIs600W] = useState(window.innerWidth <= 600);
  const [is1000W, setIs1000W] = useState(window.innerWidth <= 1000);
  const [invoices, setinvoices] = useState([]);
  const userId = user.sub;

  const getInvoices = () => {
    let config = {
      method: "get",
      maxBodyLength: Infinity,
      url: `${apilink}/Invoice?UserId=${userId}`,
      headers: {},
    };

    axios
      .request(config)
      .then((response) => {
        console.log(response.data);
        setinvoices(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  useEffect(() => {
    const handleResize = () => {
      setIs600W(window.innerWidth <= 600);
      setIs1000W(window.innerWidth <= 1000);
    };

    getInvoices();
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  const renderNavbar = () => {
    if (user && user.role === "member") {
      return <NavbarLogin />;
    } else if (user && user.role === "admin") {
      return <NavbarAdmin />;
    } else {
      return <Navbar />;
    }
  };
  return (
    <>
      <CssBaseline />
      <Grid container direction="row" minHeight="100vh">
        {renderNavbar()}
        <Grid
          item
          container
          alignItems="center"
          mt="121px"
          paddingLeft={is600W ? "10px" : is1000W ? "20px" : "70px"}
          paddingRight={is600W ? "10px" : is1000W ? "20px" : "70px"}>
          <Grid container direction="row">
            <Typography variant="h6" color="#828282">
              Home {">"}
            </Typography>
            <Typography variant="h6" color="#790B0A" marginLeft="10px">
              Invoice
            </Typography>
          </Grid>
          <Grid item container sx={{ marginTop: is600W ? "" : "32px" }}>
            <Typography variant="h4">Menu Invoice</Typography>
          </Grid>
          {is600W ? null : (
            <Grid
              item
              display="flex"
              justifyContent="space-between"
              direction={is600W ? "column" : "row"}
              paddingLeft="20px"
              paddingRight="20px"
              bgcolor="#790B0A"
              height={is600W ? "600px" : "60px"}
              alignItems={"center"}
              width={is600W ? "25%" : "100%"}
              sx={{ marginTop: "24px" }}>
              <Grid item xs={2} md={1}>
                <Typography color="#fff">No</Typography>
              </Grid>
              <Grid item xs={4} md={2}>
                <Typography color="#fff" align="center">
                  No. Invoice
                </Typography>
              </Grid>
              <Grid item xs={4} md={2}>
                <Typography color="#fff" align="center">
                  Date
                </Typography>
              </Grid>
              <Grid item xs={4} md={2}>
                <Typography color="#fff" align="center">
                  Total Course
                </Typography>
              </Grid>
              <Grid item xs={4} md={2}>
                <Typography color="#fff" align="center">
                  Total Price
                </Typography>
              </Grid>
              <Grid item xs={4} md={2}>
                <Typography color="#fff" align="center">
                  Action
                </Typography>
              </Grid>
            </Grid>
          )}
          {invoices.map((invoice, index) => {
            return (
              <InvoiceCard
                key={invoice.id}
                no={index + 1}
                noinvoice={invoice.id}
                date={invoice.date}
                tcourse={invoice.totalCourse}
                tprice={invoice.totalPrice}
              />
            );
          })}
        </Grid>
        <Grid item mt="auto">
          <Footer />
        </Grid>
      </Grid>
    </>
  );
};
