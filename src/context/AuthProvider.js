import React, { createContext, useState, useEffect } from "react";
import jwt_decode from "jwt-decode";
import { useNavigate } from "react-router";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [token, setToken] = useState(localStorage.getItem("token"));
  const [user, setUser] = useState({
    ...JSON.parse(localStorage.getItem("user")),
    isAdmin: false,
  });
  const navigate = useNavigate();

  useEffect(() => {
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      setUser(JSON.parse(storedUser));
    }
  }, []);

  const login = (newToken) => {
    setToken(newToken);
    const decoded = jwt_decode(newToken);
    const isAdmin = decoded.role === "admin";
    setUser({ ...decoded, isAdmin });
    localStorage.setItem("user", JSON.stringify({ ...decoded, isAdmin }));
  };

  const logout = () => {
    setToken(null);
    setUser(null);
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    navigate("/landingpage"); // Redirect to landingpage on logout
  };

  return (
    <AuthContext.Provider value={{ token, user, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};
